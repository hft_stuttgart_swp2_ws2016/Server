***REST API***

[![build status](https://gitlab.com/hft_stuttgart_swp2_ws2016/Server/badges/master/build.svg)](https://gitlab.com/hft_stuttgart_swp2_ws2016/Server/commits/master)

Die aktuelle Jar-Datei kann aus der Build-Übersicht [Pipelines](https://gitlab.com/hft_stuttgart_swp2_ws2016/Server/pipelines) herruntergeladen werden.

```
    java -jar ./SWP2_API-0.0.2.jar server ./config.yml
```


Inhalt der config.yml Datei:
```
        host: localhost
        server:
          rootPath: /api/
          applicationConnectors:
            - type: http
              port: 8443
        database:
          - driverClass: org.postgresql.Driver
            user: [USERNAME]
            password: [PASSWORD]
            url: jdbc:postgresql://.../db-name
        weatherApiKey: [APIKEY]
        swagger:
            resourcePackage: hft.stuttgart.swp2_api.resource
        assets:
            mappings:
              /assets: /gltf
            overrides:
              /gltf: /.../.../gltf/
        citydbExporterConfigPath: /.../.../3dcitydb/importer-exporter/config/project.xml
        citydbExporterJarPath: /.../.../importer-exporter.jar
    
```
Der OpenWeater API Key kann über folgende URL erzeugt werden [openweathermap.org](http://openweathermap.org/api).
Es wird ein Account benötigt, welcher umsonst angelegt werden kann.

***SWAGGER Dokumentation***

Aufrufen unter:

 http://[ip]:[port]/api/swagger
 
 (Swagger wird automatisch mit dem Server mit gestartet)
