/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2.util;

import hft.stuttgart.swp2_api.data.Coordinate;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class CoordinateTransformationTest {
    
    public CoordinateTransformationTest() {
    }

    /**
     * Test of wgs84ToGaussKrueger method, of class CoordinateTransformation.
     */
    @Test
    public void testWgs84ToGaussKrueger() {
        System.out.println("wgs84ToGaussKrueger");
        //Coordinate{longCoordinate=5404833.26143047, latCoordinate=3513315.24041264}
        //Coordinate{longCoordinate=5404833.271250412, latCoordinate=3513315.2405642467}
        double lon = 9.180146348132688;
        double lat = 48.780877530858284;
        Coordinate expResult = new Coordinate(5404833.271250412, 3513315.2405642467);
        Coordinate result = CoordinateTransformation.wgs84ToGaussKrueger(lon, lat);
        System.out.println(expResult.toString() + " " + result.toString());
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of gaußKrugerToWgs84 method, of class CoordinateTransformation.
     */
    @Test
    public void testGaussKrugerToWgs84() {
        System.out.println("gaussKrugerToWgs84");
        double lon = 5404833.26143047;
        double lat = 3513315.24041264;
        Coordinate expResult = new Coordinate(9.180146348132705, 48.78087753085832);
        Coordinate result = CoordinateTransformation.gaussKrugerToWgs84(lat, lon);
        System.out.println(expResult.toString() + " " + result.toString());
        assertEquals(expResult.toString(), result.toString());
    }
    
}
