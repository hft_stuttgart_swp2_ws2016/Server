/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class AbstractCafeTest {

    private AbstractCafe instance = new Cafe(
            "Test",
            1,
            new Coordinate(5404833.271250412, 3513315.2405642467),
            "test.json",
            new Address("Teststreet", "2a", "TestCity", 1234)
    );

    public AbstractCafeTest() {

    }

    /**
     * Test of getName method, of class AbstractCafe.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "Test";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class AbstractCafe.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Bla";
        instance.setName(name);
        String result = instance.getName();
        assertEquals(name, result);
    }

    /**
     * Test of getAddress method, of class AbstractCafe.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        Address expResult = new Address("Teststreet", "2a", "TestCity", 1234);
        Address result = instance.getAddress();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setAddress method, of class AbstractCafe.
     */
    @Test
    public void testSetAddress() {
        System.out.println("setAddress");
        Address address = new Address("Teststreeta", "2a", "TestCity", 1234);;
        instance.setAddress(address);
        assertEquals(instance.getAddress().toString(), address.toString());
    }

    /**
     * Test of getLocationXY method, of class AbstractCafe.
     */
    @Test
    public void testGetLocationXY() {
        System.out.println("getLocationXY");
        Coordinate expResult = new Coordinate(5404833.271250412, 3513315.2405642467);
        Coordinate result = instance.getLocationXY();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setLocationXY method, of class AbstractCafe.
     */
    @Test
    public void testSetLocationXY() {
        System.out.println("setLocationXY");
        Coordinate coordinate = new Coordinate(5404833.271250442, 3513315.2405642487);
        instance.setLocationXY(coordinate);
        assertEquals(instance.getLocationXY().toString(), coordinate.toString());
    }

    /**
     * Test of toString method, of class AbstractCafe.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Cafe{name=Test, gmlId=1, locationXY=Coordinate{longCoordinate=5404833.271250412, latCoordinate=3513315.2405642467}, serverGltfPathUrl=test.json}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
