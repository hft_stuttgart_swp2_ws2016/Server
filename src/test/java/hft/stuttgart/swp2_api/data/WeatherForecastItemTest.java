/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class WeatherForecastItemTest {

    private WeatherForecastItem forecastItem;

    public WeatherForecastItemTest() {
        this.forecastItem = new WeatherForecastItem(new Date(14789412), 12.4, "sun", 70);
    }

    /**
     * Test of getTime method, of class WeatherForecastItem.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
        Date expResult = new Date(14789412);
        Date result = forecastItem.getTime();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setTime method, of class WeatherForecastItem.
     */
    @Test
    public void testSetTime() {
        System.out.println("setTime");
        Date time = new Date(14789412);
        forecastItem.setTime(time);
        assertEquals(time.toString(), forecastItem.getTime().toString());
    }

    /**
     * Test of getCurrentTemp method, of class WeatherForecastItem.
     */
    @Test
    public void testGetCurrentTemp() {
        System.out.println("getCurrentTemp");
        double expResult = 11.4;
        double result = forecastItem.getCurrentTemp();
        assertEquals(expResult, result, 11.4);
    }

    /**
     * Test of getForecast method, of class WeatherForecastItem.
     */
    @Test
    public void testGetForecast() {
        System.out.println("getForecast");
        String expResult = "sun";
        String result = forecastItem.getForecast();
        assertEquals(expResult, result);
    }

    /**
     * Test of setForecast method, of class WeatherForecastItem.
     */
    @Test
    public void testSetForecast() {
        System.out.println("setForecast");
        String forecast = "rain";
        forecastItem.setForecast(forecast);
        assertEquals(forecast, forecastItem.getForecast());
    }

    /**
     * Test of toString method, of class WeatherForecastItem.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String result = forecastItem.toString();
        System.out.println(result);
        assertEquals(
                result,
                new WeatherForecastItem(new Date(14789412), 12.4, "sun", 70).toString()
        );
    }

    /**
     * Test of getClouds method, of class WeatherForecastItem.
     */
    @Test
    public void testGetClouds() {
        System.out.println("getClouds");
        int expResult = 70;
        int result = forecastItem.getClouds();
        assertEquals(expResult, result);
    }

    /**
     * Test of setClouds method, of class WeatherForecastItem.
     */
    @Test
    public void testSetClouds() {
        System.out.println("setClouds");
        int clouds = 0;
        forecastItem.setClouds(clouds);
        assertEquals(forecastItem.getClouds(), clouds);
    }

}
