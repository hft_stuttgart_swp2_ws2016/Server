/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author jasper
 */
public class CafeListTest {

    private LinkedList<Cafe> cafes;
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    /**
     * Constructor for the cafetest class
     */
    public CafeListTest() {
        this.cafes = new LinkedList<>();
        this.cafes.add(
                new Cafe(
                        "Königsbau Café",
                        1,
                        new Coordinate(
                                48.778959303698024,
                                9.178507588223493
                        ),
                        "www.cool.org/gltf/384.gltf",
                        new Address("Königstraße", "28", "Stuttgart", 0)
                )
        );

        this.cafes.add(
                new Cafe(
                        "coffreez",
                        2,
                        new Coordinate(
                                48.77996020989954,
                                9.17844340318474
                        ),
                        "www.cool.org/gltf/656.gltf",
                        new Address("Königstraße", "28", "Stuttgart", 0)
                )
        );
    }

    @Test
    public void serializesToJSON() throws Exception {
        CafeList cafeList = new CafeList(this.cafes);
        cafeList.setId(3);
        String result = MAPPER.writeValueAsString(cafeList);

        System.out.println(result);
        System.out.println(fixture("dataobjectsasjson/cafelist.json"));

        assertThat(fixture("dataobjectsasjson/cafelist.json")).isEqualTo(result);
    }

    /**
     * Test of getId method, of class CafeList.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        CafeList instance = new CafeList(1, cafes);
        long expResult = 1;
        long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class CafeList.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        long id = 100000;
        CafeList instance = new CafeList(1, cafes);
        instance.setId(id);
        long result = instance.getId();
        assertEquals(id, result);
    }

    /**
     * Test of getCafes method, of class CafeList.
     */
    @Test
    public void testGetCafes() {
        System.out.println("getCafes");
        CafeList instance = new CafeList(cafes);
        List<Cafe> result = instance.getCafes();
        assertEquals(cafes, result);
    }

    /**
     * Test of setCafes method, of class CafeList.
     */
    @Test
    public void testSetCafes() {
        System.out.println("setCafes");
        CafeList instance = new CafeList(new ArrayList<Cafe>());
        instance.setCafes(cafes);
        assertEquals(cafes, instance.getCafes());
    }

    /**
     * Test of toString method, of class CafeList.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CafeList instance = new CafeList(new ArrayList<Cafe>());;
        String expResult = "CafeList{id=0, cafes=[]}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
