/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class CafeWithShadowTest {

    private CafeWithShadow instance;

    public CafeWithShadowTest() {
        ArrayList<Coordinate[]> envelopes = new ArrayList<>();
        envelopes.add(new Coordinate[]{
            new Coordinate(5404833.271250412, 3513315.2405642467),
            new Coordinate(5404833.271270412, 3513615.2405642467)
        });
        this.instance = new CafeWithShadow(
                "Glücklich und satt",
                3,
                new Coordinate(
                        45.23123,
                        9.2414
                ),
                "www.cool.org/gltf/datei.gltf",
                new Address("Teststraße", "12", "Stuttgart", 0),
                envelopes
        );
        this.instance.setShadow(new ArrayList<>());
    }

    /**
     * Test of getShadow method, of class CafeWithShadow.
     */
    @Test
    public void testGetShadow() {
        System.out.println("getShadow");
        List<HashMap<String, Number>> expResult = new ArrayList<>();
        List<HashMap<String, Number>> result = instance.getShadow();
        assertEquals(expResult, result);
    }

    /**
     * Test of setShadow method, of class CafeWithShadow.
     */
    @Test
    public void testSetShadow() {
        System.out.println("setShadow");
        List<HashMap<String, Number>> shadow = new ArrayList<>();
        HashMap<String, Number> s = new HashMap<>();
        s.put("percent", 0.45);
        s.put("timestamp", 1486890000);
        shadow.add(s);
        instance.setShadow(shadow);
        assertEquals(instance.getShadow(), shadow);
    }

    /**
     * Test of getEnvelopes method, of class CafeWithShadow.
     */
    @Test
    public void testGetEnvelopes() {
        System.out.println("getEnvelopes");
        ArrayList<Coordinate[]> envelopes = new ArrayList<>();
        envelopes.add(new Coordinate[]{
            new Coordinate(5404833.271250412, 3513315.2405642467),
            new Coordinate(5404833.271270412, 3513615.2405642467)
        });
        List result = instance.getEnvelopes();
        assertEquals(((Coordinate[]) result.get(0))[0].toString(), envelopes.get(0)[0].toString()
        );
    }

    /**
     * Test of setEnvelopes method, of class CafeWithShadow.
     */
    @Test
    public void testSetEnvelopes() {
        System.out.println("setEnvelopes");
        ArrayList<Coordinate[]> envelopes = new ArrayList<>();
        envelopes.add(new Coordinate[]{
            new Coordinate(5404833.271250412, 3513315.2405642467),
            new Coordinate(5404833.271270412, 3513615.2405642467)
        });
        instance.setEnvelopes(envelopes);
        List result = instance.getEnvelopes();
        assertEquals(((Coordinate[]) result.get(0))[0].toString(), envelopes.get(0)[0].toString()
        );
    }

}
