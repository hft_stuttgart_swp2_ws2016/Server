/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class NewCafeTest {

    private NewCafe instance;

    public NewCafeTest() {
        ArrayList<Coordinate[]> envelopes = new ArrayList<>();
        envelopes.add(new Coordinate[]{
            new Coordinate(5404833.271250412, 3513315.2405642467),
            new Coordinate(5404833.271270412, 3513615.2405642467)
        });

        this.instance = new NewCafe(
                "Test",
                new Coordinate(5404833.271250412, 3513315.2405642467),
                new Address("Teststraße", "12", "Stuttgart", 1),
                envelopes
        );
    }

    /**
     * Test of getEnvelopes method, of class NewCafe.
     */
    @Test
    public void testGetEnvelopes() {
        System.out.println("getEnvelopes");
        ArrayList<Coordinate[]> envelopes = new ArrayList<>();
        envelopes.add(new Coordinate[]{
            new Coordinate(5404833.271250412, 3513315.2405642467),
            new Coordinate(5404833.271270412, 3513615.2405642467)
        });
        List result = instance.getEnvelopes();
        assertEquals(((Coordinate[]) result.get(0))[0].toString(), envelopes.get(0)[0].toString()
        );
    }

    /**
     * Test of setEnvelopes method, of class NewCafe.
     */
    @Test
    public void testSetEnvelopes() {
        System.out.println("setEnvelopes");
        ArrayList<Coordinate[]> envelopes = new ArrayList<>();
        envelopes.add(new Coordinate[]{
            new Coordinate(5404833.271250412, 3513315.2405642467),
            new Coordinate(5404833.271270412, 3513615.2405642467)
        });
        instance.setEnvelopes(envelopes);
        List result = instance.getEnvelopes();
        assertEquals(((Coordinate[]) result.get(0))[0].toString(), envelopes.get(0)[0].toString()
        );
    }

    /**
     * Test of toString method, of class NewCafe.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Cafe{name=Test, envelope=[Coordinate{longCoordinate=5404833.271250412, latCoordinate=3513315.2405642467}, Coordinate{longCoordinate=5404833.271270412, latCoordinate=3513615.2405642467}], locationXY=Coordinate{longCoordinate=5404833.271250412, latCoordinate=3513315.2405642467}, address=Address{street=Teststraße, houseNumber=12, city=Stuttgart, zip=1}}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
