/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class AddressTest {

    public AddressTest() {
    }

    private Address instance = new Address("Teststreet", "2a", "TestCity", 1234);

    /**
     * Test of getStreet method, of class Address.
     */
    @Test
    public void testGetStreet() {
        System.out.println("getStreet");
        String expResult = "Teststreet";
        String result = instance.getStreet();
        assertEquals(expResult, result);
    }

    /**
     * Test of setStreet method, of class Address.
     */
    @Test
    public void testSetStreet() {
        System.out.println("setStreet");
        String street = "Teststreetb";
        instance.setStreet(street);
        assertEquals(instance.getStreet(), street);
    }

    /**
     * Test of getHouseNumber method, of class Address.
     */
    @Test
    public void testGetHouseNumber() {
        System.out.println("getHouseNumber");
        String expResult = "2a";
        String result = instance.getHouseNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHouseNumber method, of class Address.
     */
    @Test
    public void testSetHouseNumber() {
        System.out.println("setHouseNumber");
        String houseNumber = "2b";
        instance.setHouseNumber(houseNumber);
        assertEquals(instance.getHouseNumber(), houseNumber);
    }

    /**
     * Test of getCity method, of class Address.
     */
    @Test
    public void testGetCity() {
        System.out.println("getCity");
        String expResult = "TestCity";
        String result = instance.getCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCity method, of class Address.
     */
    @Test
    public void testSetCity() {
        System.out.println("setCity");
        String city = "TestCityx";
        instance.setCity(city);
        assertEquals(instance.getCity(), city);
    }

    /**
     * Test of getZip method, of class Address.
     */
    @Test
    public void testGetZip() {
        System.out.println("getZip");
        int expResult = 1234;
        int result = instance.getZip();
        assertEquals(expResult, result);
    }

    /**
     * Test of setZip method, of class Address.
     */
    @Test
    public void testSetZip() {
        System.out.println("setZip");
        int zip = 12345;
        instance.setZip(zip);
        assertEquals(instance.getZip(), zip);
    }

    /**
     * Test of toString method, of class Address.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Address{street=Teststreet, houseNumber=2a, city=TestCity, zip=1234}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
