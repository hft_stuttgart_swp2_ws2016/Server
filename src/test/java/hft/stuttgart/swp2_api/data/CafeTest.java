/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author jasper
 */
public class CafeTest {

    private Cafe cafe;
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    public CafeTest() {
        this.cafe = new Cafe(
                "Glücklich und satt",
                3,
                new Coordinate(
                        45.23123,
                        9.2414
                ),
                "www.cool.org/gltf/datei.gltf",
                new Address("Teststraße", "12", "Stuttgart", 0)
        );
    }

    @Test
    public void serilizeObjectToJson() throws JsonProcessingException {
        String result = MAPPER.writeValueAsString(this.cafe);

        System.out.println(result);
        System.out.println(fixture("dataobjectsasjson/cafeByName.json"));

        assertThat(fixture("dataobjectsasjson/cafeByName.json")).isEqualTo(result);
    }

    /**
     * Test of getName method, of class Cafe.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "Glücklich und satt";
        String result = cafe.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Cafe.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Frisch und Fröhlich";
        cafe.setName(name);
        assertEquals(name, cafe.getName());
    }

    /**
     * Test of getAddress method, of class Cafe.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        Address expResult = new Address("Teststraße", "12", "Stuttgart", 0);
        Address result = cafe.getAddress();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setAddress method, of class Cafe.
     */
    @Test
    public void testSetAddress() {
        System.out.println("setAddress");
        Address address = new Address("Teststraße", "1", "Stuttgart", 0);
        cafe.setAddress(address);
        assertEquals(address, cafe.getAddress());
    }

    /**
     * Test of getGmlId method, of class Cafe.
     */
    @Test
    public void testGetGmlId() {
        System.out.println("getGmlId");
        int expResult = 3;
        int result = cafe.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setGmlId method, of class Cafe.
     */
    @Test
    public void testSetGmlId() {
        System.out.println("setGmlId");
        int gmlId = 4;
        cafe.setId(gmlId);
        assertEquals(cafe.getId(), gmlId);
    }

    /**
     * Test of getLocationXY method, of class Cafe.
     */
    @Test
    public void testGetLocationXY() {
        System.out.println("getLocationXY");
        Coordinate expResult = new Coordinate(
                45.23123,
                9.2414
        );
        Coordinate result = cafe.getLocationXY();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of getServerGltfPathUrl method, of class Cafe.
     */
    @Test
    public void testGetServerGltfPathUrl() {
        System.out.println("getServerGltfPathUrl");
        String expResult = "www.cool.org/gltf/datei.gltf";
        String result = cafe.getServerGltfPathUrl();
        assertEquals(expResult, result);
    }

    /**
     * Test of setServerGltfPathUrl method, of class Cafe.
     */
    @Test
    public void testSetServerGltfPathUrl() {
        System.out.println("setServerGltfPathUrl");
        String serverGltfPathUrl = "www.cool.org/gltf/datei123.gltf";
        cafe.setServerGltfPathUrl(serverGltfPathUrl);
        assertEquals(cafe.getServerGltfPathUrl(), serverGltfPathUrl);
    }

    /**
     * Test of setLocationXY method, of class Cafe.
     */
    @Test
    public void testSetLocationXY() {
        System.out.println("setLocationXY");
        Coordinate locationXY = new Coordinate(
                45.23123,
                9.2416
        );
        cafe.setLocationXY(locationXY);
        assertEquals(cafe.getLocationXY(), locationXY);
    }

    /**
     * Test of toString method, of class Cafe.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Cafe{name=Glücklich und satt, gmlId=3, locationXY=Coordinate{longCoordinate=45.23123, latCoordinate=9.2414}, serverGltfPathUrl=www.cool.org/gltf/datei.gltf}";
        String result = cafe.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Cafe.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        int expResult = 3;
        int result = cafe.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Cafe.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int gmlId = 1;
        cafe.setId(gmlId);
        assertEquals(cafe.getId(), gmlId);
    }
}
