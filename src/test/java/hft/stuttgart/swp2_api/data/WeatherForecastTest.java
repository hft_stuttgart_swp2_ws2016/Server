/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import static io.dropwizard.testing.FixtureHelpers.fixture;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class WeatherForecastTest {

    private WeatherForecast forecast;
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    public WeatherForecastTest() {
        ArrayList<WeatherForecastItem> forecastItems = new ArrayList<>();
        forecastItems.add(new WeatherForecastItem(new Date(14789412), 12.4, "sun", 50));
        this.forecast = new WeatherForecast(new Date(14789412), forecastItems);
        this.forecast.setId(0L);
        this.forecast.setCityName("Stuttgart");
    }

    @Test
    public void serilizeObjectToJson() throws JsonProcessingException {
        String result = MAPPER.writeValueAsString(this.forecast);

        System.out.println(result);
        System.out.println(fixture("dataobjectsasjson/cafeByName.json"));

        assertThat(fixture("dataobjectsasjson/weatherforcast.json")).isEqualTo(result);
    }

    /**
     * Test of getId method, of class WeatherForecast.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        long expResult = 0L;
        long result = forecast.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class WeatherForecast.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        long id = 0L;
        forecast.setId(id);
        assertEquals(forecast.getId(), id);
    }

    /**
     * Test of getDate method, of class WeatherForecast.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        Date expResult = new Date(14789412);
        Date result = forecast.getDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDate method, of class WeatherForecast.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        Date date = new Date(14789482);
        forecast.setDate(date);
        assertEquals(forecast.getDate(), date);
    }

    /**
     * Test of getCityName method, of class WeatherForecast.
     */
    @Test
    public void testGetCityName() {
        System.out.println("getCityName");
        String expResult = "Stuttgart";
        String result = forecast.getCityName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCityName method, of class WeatherForecast.
     */
    @Test
    public void testSetCityName() {
        System.out.println("setCityName");
        String cityName = "Hamburg";
        forecast.setCityName(cityName);
        assertEquals(forecast.getCityName(), cityName);
    }

    /**
     * Test of getWeatherForcastItems method, of class WeatherForecast.
     */
    @Test
    public void testGetWeatherForcastItems() {
        System.out.println("getWeatherForcastItems");
        List<WeatherForecastItem> result = forecast.getWeatherForcastItems();
        ArrayList<WeatherForecastItem> forecastItems = new ArrayList<>();
        forecastItems.add(new WeatherForecastItem(new Date(14789412), 12.4, "sun", 50));
        assertEquals(forecastItems.toString(), result.toString());
    }

    /**
     * Test of setWeatherForcastItems method, of class WeatherForecast.
     */
    @Test
    public void testSetWeatherForcastItems() {
        System.out.println("setWeatherForcastItems");
        ArrayList<WeatherForecastItem> forecastItemsTest = new ArrayList<>();
        forecastItemsTest.add(new WeatherForecastItem(new Date(14789412), 15.4, "sun", 50));
        forecast.setWeatherForcastItems(forecastItemsTest);
        assertEquals(forecastItemsTest, forecast.getWeatherForcastItems());
    }

    /**
     * Test of toString method, of class WeatherForecast.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        WeatherForecast expResult;
        ArrayList<WeatherForecastItem> forecastItems = new ArrayList<>();
        forecastItems.add(new WeatherForecastItem(new Date(14789412), 12.4, "sun", 50));
        expResult = new WeatherForecast(new Date(14789412), forecastItems);
        expResult.setId(0L);
        expResult.setCityName("Stuttgart");

        String result = forecast.toString();
        assertEquals(expResult.toString(), result);
    }

}
