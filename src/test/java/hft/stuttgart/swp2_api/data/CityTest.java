/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author 32stan1bif
 */
public class CityTest {

    private City city;

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    public CityTest() {
        city = new City("Stuttgart",
                new Coordinate(
                        48.782318,
                        9.17702
                ),
                1);
    }

    /**
     * Test of getName method, of class Cafe.
     */
    @Test
    public void serilizeToJson() throws JsonProcessingException {

        String result = MAPPER.writeValueAsString(this.city);

        System.out.println(result);
        System.out.println(fixture("dataobjectsasjson/cityByName.json"));

        assertThat(fixture("dataobjectsasjson/cityByName.json")).isEqualTo(result);
    }

    /**
     * Test of getCityId method, of class City.
     */
    @Test
    public void testGetCityId() {
        System.out.println("getCityId");
        int expResult = 1;
        int result = city.getCityId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCityId method, of class City.
     */
    @Test
    public void testSetCityId() {
        System.out.println("setCityId");
        int cityId = 2;
        city.setCityId(cityId);
        assertEquals(city.getCityId(), cityId);
    }

    /**
     * Test of getId method, of class City.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        long expResult = 0L;
        long result = city.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class City.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        long id = 0L;
        city.setId(id);
        assertEquals(id, city.getId());
    }

    /**
     * Test of getName method, of class City.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "Stuttgart";
        String result = city.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class City.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Hamburg";
        city.setName(name);
        assertEquals(name, city.getName());
    }

    /**
     * Test of getCoordinate method, of class City.
     */
    @Test
    public void testGetCoordinate() {
        System.out.println("getCoordinate");
        Coordinate expResult = new Coordinate(
                48.782318,
                9.17702
        );
        Coordinate result = city.getCoordinate();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setCoordinate method, of class City.
     */
    @Test
    public void testSetCoordinate() {
        System.out.println("setCoordinate");
        Coordinate coordinate = new Coordinate(
                48.782318,
                9.17701
        );
        city.setCoordinate(coordinate);
        assertEquals(city.getCoordinate().toString(), coordinate.toString());
    }

    /**
     * Test of toString method, of class City.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "City{name=Stuttgart, coordinate=Coordinate{longCoordinate=48.782318, latCoordinate=9.17702}}";
        String result = city.toString();
        assertEquals(expResult, result);
    }
}
