/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.data;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class CoordinateTest {

    private Coordinate coordinate;

    public CoordinateTest() {
        this.coordinate = new Coordinate(
                45.23123,
                9.2414
        );
    }

    /**
     * Test of getLongCoordinate method, of class Coordinate.
     */
    @Test
    public void testGetLongCoordinate() {
        System.out.println("getLongCoordinate");
        double expResult = 45.23123;
        double result = coordinate.getLongCoordinate();
        assertEquals(expResult, result, 45.23123);
    }

    /**
     * Test of setLongCoordinate method, of class Coordinate.
     */
    @Test
    public void testSetLongCoordinate() {
        System.out.println("setLongCoordinate");
        double longCoordinate = 45.23123;
        coordinate.setLongCoordinate(longCoordinate);
        assertEquals(coordinate.getLongCoordinate(), longCoordinate, 45.23123);
    }

    /**
     * Test of getLatCoordinate method, of class Coordinate.
     */
    @Test
    public void testGetLatCoordinate() {
        System.out.println("getLatCoordinate");;
        double expResult = 9.2414;
        double result = coordinate.getLatCoordinate();
        assertEquals(expResult, result, 9.2414);
    }

    /**
     * Test of setLatCoordinate method, of class Coordinate.
     */
    @Test
    public void testSetLatCoordinate() {
        System.out.println("setLatCoordinate");
        double latCoordinate = 9.2414;
        coordinate.setLatCoordinate(latCoordinate);
        assertEquals(coordinate.getLatCoordinate(), latCoordinate, 9.2414);
    }

    /**
     * Test of toString method, of class Coordinate.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String result = coordinate.toString();
        System.out.println(result);
        assertEquals(result, "Coordinate{longCoordinate=45.23123, "
                + "latCoordinate=9.2414}");
    }

}
