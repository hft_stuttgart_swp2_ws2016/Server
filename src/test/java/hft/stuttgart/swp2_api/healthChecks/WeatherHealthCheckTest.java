/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.healthChecks;

import com.codahale.metrics.health.HealthCheck;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class WeatherHealthCheckTest {

    public WeatherHealthCheckTest() {
    }

    /**
     * Test of check method, of class WeatherHealthCheck.
     */
    @Test
    public void testCheckTrue() throws Exception {
        System.out.println("check");
        WeatherHealthCheck instance = new WeatherHealthCheck("sjdlj898sdf");
        HealthCheck.Result result = instance.check();
        assertEquals(instance.check().isHealthy(), true);
    }

    /**
     * Test of check method, of class WeatherHealthCheck.
     */
    @Test
    public void testCheckFalse() throws Exception {
        System.out.println("check");
        WeatherHealthCheck instance = new WeatherHealthCheck(null);
        HealthCheck.Result result = instance.check();
        assertEquals(result.isHealthy(), false);
    }

    /**
     * Test of check method, of class WeatherHealthCheck.
     */
    @Test
    public void testCheck() throws Exception {
        System.out.println("check");
        WeatherHealthCheck instance = new WeatherHealthCheck(null);
        HealthCheck.Result result = instance.check();
        assertEquals(result, result);
    }
}
