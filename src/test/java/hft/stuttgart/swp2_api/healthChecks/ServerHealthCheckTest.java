/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.healthChecks;

import com.codahale.metrics.health.HealthCheck;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class ServerHealthCheckTest {

    public ServerHealthCheckTest() {
    }

    /**
     * Test of check method, of class ServerHealthCheck.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCheckTrue() throws Exception {

        System.out.println("check");
        ServerHealthCheck instance = new ServerHealthCheck("localhost", 8080);
        HealthCheck.Result result = instance.check();
        assertEquals(result.isHealthy(), true);
    }

    /**
     * Test of check method, of class ServerHealthCheck.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCheckFalse() throws Exception {
        System.out.println("check");
        ServerHealthCheck instance = new ServerHealthCheck(null, 0);
        HealthCheck.Result result = instance.check();
        assertEquals(result.isHealthy(), false);
    }

    /**
     * Test of check method, of class ServerHealthCheck.
     */
    @Test
    public void testCheck() throws Exception {
        System.out.println("check");
        ServerHealthCheck instance = new ServerHealthCheck(null, 0);
        HealthCheck.Result result = instance.check();
        assertEquals(result, result);
    }
}
