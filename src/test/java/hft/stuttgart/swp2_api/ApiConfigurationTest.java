/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api;

import io.dropwizard.bundles.assets.AssetsConfiguration;
import io.dropwizard.db.DataSourceFactory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class ApiConfigurationTest {

    public ApiConfigurationTest() {
    }

    /**
     * Test of getHost method, of class ApiConfiguration.
     */
    @Test
    public void testGetHost() {
        System.out.println("getHost");
        ApiConfiguration instance = new ApiConfiguration();
        instance.setHost("localhost");
        String result = instance.getHost();
        assertEquals("localhost", result);
    }

    /**
     * Test of setHost method, of class ApiConfiguration.
     */
    @Test
    public void testSetHost() {
        System.out.println("setHost");
        ApiConfiguration instance = new ApiConfiguration();
        instance.setHost("localhost");
        String result = instance.getHost();
        assertEquals("localhost", result);
    }

    /**
     * Test of getPort method, of class ApiConfiguration.
     */
    @Test
    public void testGetPort() {
        System.out.println("getPort");
        ApiConfiguration instance = new ApiConfiguration();
        instance.setPort(80);
        int result = instance.getPort();
        assertEquals(80, result);
    }

    /**
     * Test of setPort method, of class ApiConfiguration.
     */
    @Test
    public void testSetPort() {
        System.out.println("setPort");
        ApiConfiguration instance = new ApiConfiguration();
        instance.setPort(80);
        int result = instance.getPort();
        assertEquals(80, result);
    }

    /**
     * Test of getWeatherApiKey method, of class ApiConfiguration.
     */
    @Test
    public void testGetWeatherApiKey() {
        System.out.println("getWeatherApiKey");
        ApiConfiguration instance = new ApiConfiguration();
        instance.setWeatherApiKey("asdfg");
        String result = instance.getWeatherApiKey();
        assertEquals("asdfg", result);
    }

    /**
     * Test of setWeatherApiKey method, of class ApiConfiguration.
     */
    @Test
    public void testSetWeatherApiKey() {
        System.out.println("setWeatherApiKey");
        ApiConfiguration instance = new ApiConfiguration();
        instance.setWeatherApiKey("asdfg");
        String result = instance.getWeatherApiKey();
        assertEquals("asdfg", result);
    }

    /**
     * Test of setDataSourceFactory method, of class ApiConfiguration.
     */
    @Test
    public void testSetDataSourceFactory() {
        System.out.println("setDataSourceFactory");
        DataSourceFactory factory = new DataSourceFactory();
        ApiConfiguration instance = new ApiConfiguration();
        instance.setDataSourceFactory(factory);
        assertEquals(factory, instance.getDataSourceFactory());
    }

    /**
     * Test of getDataSourceFactory method, of class ApiConfiguration.
     */
    @Test
    public void testGetDataSourceFactory() {
        System.out.println("getDataSourceFactory");
        ApiConfiguration instance = new ApiConfiguration();
        DataSourceFactory expResult = new DataSourceFactory();
        instance.setDataSourceFactory(expResult);
        DataSourceFactory result = instance.getDataSourceFactory();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAssetsConfiguration method, of class ApiConfiguration.
     */
    @Test
    public void testGetAssetsConfiguration() {
        System.out.println("getAssetsConfiguration");
        ApiConfiguration instance = new ApiConfiguration();
        DataSourceFactory expResult = new DataSourceFactory();
        instance.setDataSourceFactory(expResult);
        DataSourceFactory result = instance.getDataSourceFactory();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCitydbExporterConfigPath method, of class ApiConfiguration.
     */
    @Test
    public void testGetCitydbExporterConfigPath() {
        System.out.println("getCitydbExporterConfigPath");
        ApiConfiguration instance = new ApiConfiguration();
        String expResult = "bla";
        instance.setCitydbExporterConfigPath(expResult);
        String result = instance.getCitydbExporterConfigPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCitydbExporterConfigPath method, of class ApiConfiguration.
     */
    @Test
    public void testSetCitydbExporterConfigPath() {
        System.out.println("setCitydbExporterConfigPath");
        ApiConfiguration instance = new ApiConfiguration();
        String expResult = "bla";
        instance.setCitydbExporterConfigPath(expResult);
        String result = instance.getCitydbExporterConfigPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCitydbExporterJarPath method, of class ApiConfiguration.
     */
    @Test
    public void testGetCitydbExporterJarPath() {
        System.out.println("getCitydbExporterJarPath");
        ApiConfiguration instance = new ApiConfiguration();
        String expResult = "blub";
        instance.setCitydbExporterJarPath(expResult);
        String result = instance.getCitydbExporterJarPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCitydbExporterJarPath method, of class ApiConfiguration.
     */
    @Test
    public void testSetCitydbExporterJarPath() {
        System.out.println("setCitydbExporterJarPath");
        String citydbExporterJarPath = "wuff";
        ApiConfiguration instance = new ApiConfiguration();
        instance.setCitydbExporterConfigPath(citydbExporterJarPath);
        instance.setCitydbExporterJarPath(citydbExporterJarPath);
    }
}
