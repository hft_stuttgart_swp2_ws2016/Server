/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.services;

import hft.stuttgart.swp2_api.data.City;
import hft.stuttgart.swp2_api.exceptions.CityNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jasper
 */
public class CityServiceTest {

    public CityServiceTest() {
    }

    /**
     * Test of getCityByQuery method, of class CityService.
     */
    @Test
    public void testGetCityByQuery() {
        System.out.println("getCityByQuery");
        String cityQuery = "";
        CityService instance = new CityService(cityQuery);
        City result = instance.getCityByQuery(cityQuery);
        assertEquals(null, result);
    }

    /**
     * Test of convertJsonToCity method, of class CityService.
     * 
     * @throws java.lang.Exception
     */
    @Test(expected = CityNotFoundException.class)
    public void testConvertJsonToCity() throws Exception {
        System.out.println("convertJsonToCity");
        String jsonCity = "";
        City expResult = null;
        City result = CityService.convertJsonToCity(jsonCity);
        assertEquals(expResult, result);
    }

}
