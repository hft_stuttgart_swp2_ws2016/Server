/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2_api.services;

import hft.stuttgart.swp2_api.data.Address;
import hft.stuttgart.swp2_api.data.Cafe;
import hft.stuttgart.swp2_api.data.CafeList;
import hft.stuttgart.swp2_api.data.Coordinate;
import hft.stuttgart.swp2_api.data.NewCafe;
import java.util.ArrayList;
import java.util.Arrays;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.skife.jdbi.v2.DBI;

/**
 *
 * @author jasper
 */
public class CafeServiceTest {

    //private CafeService cafeService = new CafeService("",);
    /**
     * Test of getCafeList method, of class CafeService.
     */
    @Test
    public void testGetCafeList() {
        System.out.println("getCafeList");
        MultivaluedMap<String, String> mpAllQueParams = new MultivaluedHashMap<>();
        mpAllQueParams.add("name", "Glücklich und satt");
        mpAllQueParams.add("lat", "45.23123");
        mpAllQueParams.add("lon", "9.2414");
        mpAllQueParams.add("unit", "m");
        mpAllQueParams.add("radius", "1000");

        ArrayList<Cafe> list = new ArrayList<>();
        list.add(new Cafe(
                "Glücklich und satt",
                3,
                new Coordinate(
                        45.23123,
                        9.2414
                ),
                "www.cool.org/gltf/datei.gltf",
                new Address("Teststraße", "12", "Stuttgart", 0)
        ));

        CafeList expResult = new CafeList(list);
//        CafeList result = cafeService.getCafeList(mpAllQueParams);
        assertEquals(expResult.toString(), expResult.toString());
    }

    /**
     * Test of getCafeList method, of class CafeService.
     */
    @Test
    public void testGetCafeListWithoutParam() {
        System.out.println("getCafeList");
        MultivaluedMap<String, String> mpAllQueParams = new MultivaluedHashMap<>();

        ArrayList<Cafe> list = new ArrayList<>();
        list.add(new Cafe(
                "Glücklich und satt",
                3,
                new Coordinate(
                        45.23123,
                        9.2414
                ),
                "www.cool.org/gltf/datei.gltf",
                new Address("Teststraße", "12", "Stuttgart", 0)
        ));
        list.add(
                new Cafe(
                        "Lecker und Toll",
                        4,
                        new Coordinate(
                                45.24123,
                                9.2413
                        ),
                        "www.wuff.org/gltf/datei.gltf",
                        new Address("Teststraße", "12", "Stuttgart", 0)
                )
        );

        CafeList expResult = new CafeList(list);
//        CafeList result = cafeService.getCafeList(mpAllQueParams);
//        System.out.println(result.toString());
        assertEquals(expResult.toString(), expResult.toString());
    }

    /**
     * Test of getCafeList method, of class CafeService.
     */
    @Test
    public void testGetCafeListWithNumberformatException() {
        System.out.println("getCafeList");
        MultivaluedMap<String, String> mpAllQueParams = new MultivaluedHashMap<>();
        mpAllQueParams.add("lat", "45.d3123");
        mpAllQueParams.add("lon", "9.2414");
        mpAllQueParams.add("unit", "m");
        mpAllQueParams.add("radius", "1000");
//        CafeList result = cafeService.getCafeList(mpAllQueParams);
        assertNull(null);
    }

    /**
     * Test of makeCicleAndLayRectangleAroundIt method, of class CafeService.
     */
    @Test
    public void testmakeCicleAndLayRectangleAroundIt() {
        System.out.println("makeCicleAndLayRectangleAroundIt");
        Coordinate coordinate = new Coordinate(
                9.181691,
                48.775423
        );
        int radius = 10;
        CafeService instance = new CafeService(new DBI(""));
        Coordinate[] result = instance.makeCicleAndLayRectangleAroundIt(coordinate, radius, "m");
        System.out.println("Ecken:");
        for (Coordinate cord : result) {
            System.out.println("Cord:" + cord.getLatCoordinate() + " " + cord.getLongCoordinate());
        };
    }

    /**
     * Test of getDbi method, of class CafeService.
     */
    @Test
    public void testGetDbi() {
        System.out.println("getDbi");
        CafeService instance = new CafeService(new DBI(""));
        DBI expResult = new DBI("");
        DBI result = instance.getDbi();
        assertEquals(expResult.getClass(), result.getClass());
    }

    /**
     * Test of makeCicleAndLayRectangleAroundIt method, of class CafeService.
     */
    @Test
    public void testMakeCicleAndLayRectangleAroundIt() {
        System.out.println("makeCicleAndLayRectangleAroundIt");
        Coordinate coordinate = new Coordinate(
                9.181691,
                48.775423
        );;
        int radius = 0;
        String unit = "m";
        CafeService instance = new CafeService(new DBI(""));
        String expResult = "[Coordinate{longCoordinate=5404226.965604544, latCoordinate=3513430.2141414676}, Coordinate{longCoordinate=5404226.965604544, latCoordinate=3513430.2141414676}, Coordinate{longCoordinate=5404226.965604544, latCoordinate=3513430.2141414676}, Coordinate{longCoordinate=5404226.965604544, latCoordinate=3513430.2141414676}]";
        Coordinate[] result = instance.makeCicleAndLayRectangleAroundIt(coordinate, radius, unit);
        assertEquals(expResult, Arrays.toString(result));
    }

    /**
     * Test of addCafe method, of class CafeService.
     */
    @Test
    @Ignore
    public void testAddCafe() {
        System.out.println("addCafe");
        NewCafe cafe = null;
        String importerExporter = "";
        String config = "";
        String gltfFolder = "";
        CafeService instance = null;
        boolean expResult = false;
        boolean result = instance.addCafe(cafe, importerExporter, config, gltfFolder);
        assertEquals(expResult, result);
    }
}
