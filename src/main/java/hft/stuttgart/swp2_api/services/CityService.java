package hft.stuttgart.swp2_api.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hft.stuttgart.swp2_api.data.City;
import hft.stuttgart.swp2_api.data.Coordinate;
import hft.stuttgart.swp2_api.exceptions.CityNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CityService {

    private String weatherApiKey;

    public CityService(String weatherApiKey) {
        this.weatherApiKey = weatherApiKey;
    }

    /**
     * This methode calls the weather api with a special city request, the api
     * will responde a Json, with this Json we will call " convertJsonToCity" to
     * make a City Object out of the Json Datas. after this the getCityByQuery
     * will return a City that dropwizard will convert to a json and respond to
     * the client
     * 
     * @param cityQuery String for example Stuttgart,DE
     * 
     * @return City object
     */
    public City getCityByQuery(String cityQuery) {
        StringBuilder jsonResponse = new StringBuilder();

        try {
            URL url = new URL(
                    "http://api.openweathermap.org/data/2.5/forecast/city?q="
                    + URLEncoder.encode(cityQuery, "UTF-8")
                    + "&APPID="
                    + this.weatherApiKey
            );

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(url.openStream(), "UTF-8")
            );
            for (String line; (line = reader.readLine()) != null;) {
                jsonResponse.append(line);
            }
        } catch (MalformedURLException | UnsupportedEncodingException ex) {
            Logger.getLogger(CityService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CityService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (jsonResponse.length() > 0) {
            try {
                return convertJsonToCity(jsonResponse.toString());
            } catch (CityNotFoundException ex) {
                Logger.getLogger(CityService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public static City convertJsonToCity(String jsonCity) throws CityNotFoundException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(jsonCity).path("city");
            JsonNode coord = rootNode.path("coord");
            double lat = coord.path("lat").asDouble();
            double lon = coord.path("lon").asDouble();

            City city = new City(
                    rootNode.path("name").asText(),
                    new Coordinate(
                            lon,
                            lat
                    ),
                    rootNode.path("id").asInt()
            );

            return city;
        } catch (IOException ex) {
            Logger.getLogger(CityService.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new CityNotFoundException();

    }
}
