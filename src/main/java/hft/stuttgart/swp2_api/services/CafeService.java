package hft.stuttgart.swp2_api.services;

import hft.stuttgart.swp2.util.CoordinateTransformation;
import hft.stuttgart.swp2.util.GltfExportUtil;
import hft.stuttgart.swp2_api.dao.CafeAreaDao;
import hft.stuttgart.swp2_api.dao.CafeDao;
import hft.stuttgart.swp2_api.dao.ShadowDao;
import hft.stuttgart.swp2_api.data.Address;
import hft.stuttgart.swp2_api.data.CafeList;
import hft.stuttgart.swp2_api.data.CafeWithShadow;
import hft.stuttgart.swp2_api.data.Coordinate;
import hft.stuttgart.swp2_api.data.NewCafe;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

/**
 * The CafeService class defines all methods to create or select cafes
 *
 * @author Andreas Strange, Jasper Eumann
 */
public class CafeService {

    /**
     * dbi DBI 
     */
    private final DBI dbi;

    /**
     * Constructor of the cafeservice class
     *
     * @param dbi DBI
     */
    public CafeService(DBI dbi) {
        this.dbi = dbi;
    }

    /**
     * Method to get cafe list
     *
     * @param name String
     * @param address String
     * @param lat double lat part of the coordinate of the center of the square
     * @param lon double long part of the coordinate of the center of the square
     * @param radius of the square as int 
     * @param unit String m or km
     * @param start long unix timestamp of the search intervall
     * @param end long unix timestamp if not set but start is set end is default start + 3 days 
     * 
     * @return CafeList or null
     */
    public CafeList getCafeList(String name, String address, double lat, double lon, int radius, String unit, long start, long end) {
        CafeList result;

        if (start > 0 && end == 0){
            end = start + 3 * 24 * 60 * 60 * 1000;
        }
        
        if (name != null && name.length() >= 1 && start > 0 && end > 0) {
            result = this.queryCafeByNameAndSun(name, start, end);
        } else if (name != null && name.length() >= 1) {
            result = this.queryCafeByName(name);
        } else if (address != null && address.length() > 1 && start > 0 && end > 0) {
            result = this.queryCafeByAddressAndSun(address, start, end);
        } else if (address != null && address.length() > 1) {
            result = this.queryCafeByAddress(address);
        } else if (address != null && address.length() > 1
                && lat > 0 && lon > 0 && start > 0 && end > 0) {
            result = this.queryCafeByLatLonAndAddressAndSun(new Coordinate(lon, lat), radius, unit, start, end, address);
        } else if (address != null && address.length() > 1
                && lat > 0 && lon > 0 && start > 0 && end > 0) {
            result = this.queryCafeByLatLonAndAddress(new Coordinate(lon, lat), radius, unit, address);
        } else if (lat > 0 && lon > 0 && start > 0 && end > 0) {
            result = this.queryCafeByLatLonAndSun(new Coordinate(lon, lat), radius, unit, start, end);
        } else if (lat > 0 && lon > 0) {
            result = this.queryCafeByLatLon(new Coordinate(lon, lat), radius, unit);
        } else if (start > 0 && end > 0) {
            result = this.queryCafeBySun(start, end);
        } else {
            result = this.queryCafes();
        }

        return result;
    }

    /**
     * Method to find cafe's by a given name
     *
     * @param name String
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByName(String name) {
        CafeList cafeList;
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            cafeList = new CafeList(cafeDao.findCafeByAddress(
                    "%<BuildingName>%" + name + "%</BuildingName>%"
            ));
        }
        return cafeList;
    }

    /**
     * Method to find cafe's by given name and with less shadow then 50% on the cafe area 
     *
     * @param name String
     * @param start long unix timestamp of the search intervall
     * @param end long unix timestamp of the search intervall
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByNameAndSun(String name, long start, long end) {
        CafeList cafeList;

        DateTime dtStart = new DateTime(start);
        DateTime dtEnd = new DateTime(end);

        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            cafeList = new CafeList(
                    cafeDao.findCafesByAddressAndSun(
                            "%<BuildingName>%" + name + "%</BuildingName>%", dtStart, dtEnd
                    )
            );

            addShadowToCafes(h, cafeList, dtStart, dtEnd);
        }
        return cafeList;
    }

    /**
     * Method to find cafe's by given address
     *
     * @param address String
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByAddress(String address) {
        CafeList cafeList;
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            String[] addressArray = address.replaceAll(",", "").split(" ");
            cafeList = new CafeList(cafeDao.findCafeByAddress(
                    genAddressQuery(addressArray)
            ));
        }

        return cafeList;
    }

    /**
     * Helper method to generate the xml query string
     *
     * @param addressArray Strin array
     * 
     * @return xml String
     */
    private String genAddressQuery(String[] addressArray) {
        String addressQuery = "";
        if (addressArray.length == 1) {
            addressQuery = "%<xAL:ThoroughfareName>%"
                    + addressArray[0] + "%</xAL:ThoroughfareName>%";
        } else if (addressArray.length == 2) {
            addressQuery = "%<xAL:ThoroughfareNumber>%" + addressArray[1]
                    + "%</xAL:ThoroughfareNumber>%<xAL:ThoroughfareName>%"
                    + addressArray[0] + "%</xAL:ThoroughfareName>%";
        } else if (addressArray.length > 2) {
            addressQuery = "%<xAL:LocalityName>%" + addressArray[2]
                    + "%</xAL:LocalityName>%<xAL:ThoroughfareNumber>%"
                    + addressArray[1]
                    + "%</xAL:ThoroughfareNumber>%<xAL:ThoroughfareName>%"
                    + addressArray[0] + "%</xAL:ThoroughfareName>%";
        }
        return addressQuery;
    }

    /**
     * Method to find cafe's by a given rectangle
     * 
     * @param coordinate Coordinate center of the square
     * @param radius int of the square 
     * @param unit String m or km default m
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByLatLon(Coordinate coordinate, int radius, String unit) {
        CafeList cafeList;
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            //here we transform from wgs84 into gauss Kruger, from now on we only have gauß krueger coordinates
            Coordinate[] listOfEdgePoints = makeCicleAndLayRectangleAroundIt(coordinate, radius, unit);
            //x1 y1 x2 y2
            System.out.println("x1 " + listOfEdgePoints[0].getLongCoordinate() + " y1 " + listOfEdgePoints[3].getLatCoordinate() + " x2 "
                    + listOfEdgePoints[1].getLongCoordinate() + " y2 "
                    + listOfEdgePoints[2].getLatCoordinate());
            cafeList = new CafeList(cafeDao.findCafeByLatLon(
                    listOfEdgePoints[0].getLongCoordinate(),
                    listOfEdgePoints[3].getLatCoordinate(),
                    listOfEdgePoints[1].getLongCoordinate(),
                    listOfEdgePoints[2].getLatCoordinate()
            ));
        }
        return cafeList;
    }

    /**
     * Method to create a rectangle from given coordinate and radius
     *
     * @param coordinate center Coordiante of the square
     * @param radius of the square
     * @param unit String m or km default m
     * 
     * @return Coordinate array
     */
    public Coordinate[] makeCicleAndLayRectangleAroundIt(Coordinate coordinate, int radius, String unit) {
        if ("km".equals(unit)) {
            radius = radius * 1000;
        }

        Coordinate centerposition = CoordinateTransformation.wgs84ToGaussKrueger(
                coordinate.getLongCoordinate(),
                coordinate.getLatCoordinate()
        );

        Coordinate[] gaussKrugerRectangleList = new Coordinate[4];

        // now we make a Circle
        //leftPointCircle 
        gaussKrugerRectangleList[0] = new Coordinate(
                centerposition.getLongCoordinate() - radius, centerposition.getLatCoordinate()
        );
        //rightPointCircle 
        gaussKrugerRectangleList[1] = new Coordinate(
                centerposition.getLongCoordinate() + radius, centerposition.getLatCoordinate()
        );
        // topPointCircle 
        gaussKrugerRectangleList[2] = new Coordinate(
                centerposition.getLongCoordinate(), centerposition.getLatCoordinate() + radius
        );
        // bottomPointCircle 
        gaussKrugerRectangleList[3] = new Coordinate(
                centerposition.getLongCoordinate(), centerposition.getLatCoordinate() - radius
        );

        return gaussKrugerRectangleList;
    }

    /**
     * Method to find cafe's by given address and without shadow between start
     * and end timestamp
     *
     * @param address String 
     * @param start long unix timestamp of the search intervall
     * @param end long unix timestamp of the search intervall
     * @return CafeList or null
     */
    private CafeList queryCafeByAddressAndSun(String address, long start, long end) {
        CafeList cafeList;
        String[] addressArray = address.replaceAll(",", "").split(" ");
        DateTime dtStart = new DateTime(start);
        DateTime dtEnd = new DateTime(end);

        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            cafeList = new CafeList(cafeDao.findCafesByAddressAndSun(
                    this.genAddressQuery(addressArray), dtStart, dtEnd)
            );

            addShadowToCafes(h, cafeList, dtStart, dtEnd);
        }
        return cafeList;
    }

    /**
     * Add Shadow arrays to Cafes
     *
     * @param h Database Handler
     * @param cafeList CafeListe
     * @param dtStart Start Datetime of search intervall
     * @param dtEnd End Datetime of search intervall
     */
    private void addShadowToCafes(final Handle h, CafeList cafeList, DateTime dtStart, DateTime dtEnd) {
        ShadowDao shadowDao = h.attach(ShadowDao.class);
        CafeAreaDao cafeAreaDao = h.attach(CafeAreaDao.class);

        cafeList.getCafes().forEach((c) -> {
            ((CafeWithShadow) c).setShadow(shadowDao.getShadowByCafeIdAndStartEnd(c.getId(), dtStart, dtEnd));
            ((CafeWithShadow) c).setEnvelopes(cafeAreaDao.findCafesByAddressAndLatLon(c.getId()));
        });    
    }

    /**
     * Method to query cafe's by lat, lon and sun
     *
     * @param coordinate center Coordinate of the square
     * @param radius int of the square
     * @param unit String m or km default km
     * @param start long unix timestamp of the search intervall
     * @param end long unix timestamp of the search intervall
     * @param address String
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByLatLonAndAddressAndSun(
            Coordinate coordinate, int radius, String unit, long start, long end, String address
    ) {
        CafeList cafeList;
        DateTime dtStart = new DateTime(start);
        DateTime dtEnd = new DateTime(end);
        String[] addressArray = address.replaceAll(",", "").split(" ");
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            Coordinate[] listOfEdgePoints = makeCicleAndLayRectangleAroundIt(coordinate, radius, unit);
            cafeList = new CafeList(cafeDao.findCafesByAddressAndLatLonAndSun(
                    this.genAddressQuery(addressArray),
                    dtStart,
                    dtEnd,
                    listOfEdgePoints[0].getLongCoordinate(),
                    listOfEdgePoints[3].getLatCoordinate(),
                    listOfEdgePoints[1].getLongCoordinate(),
                    listOfEdgePoints[2].getLatCoordinate())
            );
        }
        return cafeList;
    }

    /**
     * Method to query cafe's by lat, lon and address
     *
     * @param coordinate Coordinate of the square
     * @param radius of the square
     * @param unit String m or km default m
     * @param address String
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByLatLonAndAddress(Coordinate coordinate, int radius, String unit, String address) {
        CafeList cafeList;
        String[] addressArray = address.replaceAll(",", "").split(" ");
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            Coordinate[] listOfEdgePoints = makeCicleAndLayRectangleAroundIt(coordinate, radius, unit);
            cafeList = new CafeList(cafeDao.findCafesByAddressAndLatLon(
                    this.genAddressQuery(addressArray),
                    listOfEdgePoints[0].getLongCoordinate(),
                    listOfEdgePoints[3].getLatCoordinate(),
                    listOfEdgePoints[1].getLongCoordinate(),
                    listOfEdgePoints[2].getLatCoordinate()));
        }
        return cafeList;
    }

    /**
     * Methode to find cafe's by given coordinate and without shadow between
     * start and end timestamp
     *
     * @param coordinate Coordinate center of the square
     * @param radius of the square
     * @param unit String m or km default m
     * @param start long unix timestamp of the search intervall
     * @param end long unix timestamp of the search intervall
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeByLatLonAndSun(Coordinate coordinate, int radius, String unit, long start, long end) {
        CafeList cafeList;
        DateTime dtStart = new DateTime(start);
        DateTime dtEnd = new DateTime(end);

        //here we transform from wgs84 into gauß Kruger, from now on we only have gauß krüger coordinates
        Coordinate[] listOfEdgePoints = makeCicleAndLayRectangleAroundIt(coordinate, radius, unit);

        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            cafeList = new CafeList(cafeDao.findCafeByLatLonAndSun(
                    listOfEdgePoints[0].getLongCoordinate(),
                    listOfEdgePoints[3].getLatCoordinate(),
                    listOfEdgePoints[1].getLongCoordinate(),
                    listOfEdgePoints[2].getLatCoordinate(),
                    dtStart,
                    dtEnd)
            );

            addShadowToCafes(h, cafeList, dtStart, dtEnd);
        }
        return cafeList;
    }

    /**
     * Method to find cafe's without shadow between start and end timestamp
     *
     * @param start long unix timestamp of the search intervall
     * @param end long unix timestamp of the search intervall
     * 
     * @return CafeList or null
     */
    private CafeList queryCafeBySun(long start, long end) {
        CafeList cafeList;
        DateTime dtStart = new DateTime(start);
        DateTime dtEnd = new DateTime(end);
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            cafeList = new CafeList(cafeDao.findCafesBySun(dtStart, dtEnd));

            addShadowToCafes(h, cafeList, dtStart, dtEnd);
        }

        return cafeList;
    }

    /**
     * Method return all cafes
     *
     * @return CafeList or null
     */
    private CafeList queryCafes() {
        CafeList cafeList;
        try (Handle h = this.getDbi().open()) {
            CafeDao cafeDao = h.attach(CafeDao.class);
            cafeList = new CafeList(cafeDao.findCafes());
        }
        return cafeList;
    }

    /**
     * Getter method for the dbi instance
     *
     * @return dbi DBI
     */
    public DBI getDbi() {
        return dbi;
    }

    /**
     * Method to add a new cafe
     *
     * @param cafe NewCafe
     * @param importerExporter path as String
     * @param config path as String
     * @param gltfFolder path as String
     * 
     * @return boolean true if cafe added else false
     */
    public boolean addCafe(NewCafe cafe, String importerExporter, String config, String gltfFolder) {
        try (Handle h = this.getDbi().open()) {
            Coordinate gaussKruegerCor = CoordinateTransformation.wgs84ToGaussKrueger(
                    cafe.getLocationXY().getLongCoordinate(),
                    cafe.getLocationXY().getLatCoordinate()
            );

            CafeDao cafeDao = h.attach(CafeDao.class);
            CafeAreaDao cafeAreaDao = h.attach(CafeAreaDao.class);
        
            long cafeId = cafeDao.insertCafe(
                    this.buildXalSource(cafe.getName(), cafe.getAddress()),
                    gaussKruegerCor.getLongCoordinate(),
                    gaussKruegerCor.getLatCoordinate()
            );
            
            if (cafeId == 0){
                return false;
            }
            
            cafe.getEnvelopes().stream().map((ca) -> {
                String[] gaussKruegerEnvelope = new String[ca.length];
                for (int i = 0; i < ca.length; i++) {
                    Coordinate c = CoordinateTransformation.wgs84ToGaussKrueger(
                            ca[i].getLongCoordinate(),
                            ca[i].getLatCoordinate()
                    );
                    gaussKruegerEnvelope[i] = c.getLatCoordinate() + ", " + c.getLongCoordinate();
                }
                return gaussKruegerEnvelope;                
            }).forEachOrdered((gaussKruegerEnvelope) -> {
                cafeAreaDao.insertCafe(cafeId, StringUtils.join(gaussKruegerEnvelope, "; "));
            });

            //create glft file
            GltfExportUtil exportUtil = new GltfExportUtil(importerExporter, config, gltfFolder);
            Coordinate[] boundingBox = makeCicleAndLayRectangleAroundIt(cafe.getLocationXY(), 100, "m");
            System.out.println("ID: " + cafeId + " " + Arrays.toString(boundingBox));
            exportUtil.createGltfFiles(cafeId, boundingBox);
            
            return true;
        }
    }

    /**
     * Method to build the address xml string
     *
     * @param name String
     * @param address Address
     * 
     * @return xml String
     */
    private String buildXalSource(String name, Address address) {
        return " <xAL:AddressDetails xmlns:xAL=\"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0\"><xAL:Locality><xAL:LocalityName>"
                + "Stuttgart" + " " + address.getZip()
                + "</xAL:LocalityName><xAL:Thoroughfare Type=\"Street\"><xAL:ThoroughfareNumber>"
                + address.getHouseNumber()
                + "</xAL:ThoroughfareNumber><xAL:ThoroughfareName>"
                + address.getStreet()
                + "</xAL:ThoroughfareName></xAL:Thoroughfare><xAL:Premise Type=\"Building\">"
                + "<BuildingName>"
                + name
                + "</BuildingName></xAL:Locality></xAL:AddressDetails>";
    }
}
