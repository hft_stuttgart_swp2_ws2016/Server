package hft.stuttgart.swp2_api.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hft.stuttgart.swp2_api.data.WeatherForecast;
import hft.stuttgart.swp2_api.data.WeatherForecastItem;
import hft.stuttgart.swp2_api.exceptions.WeatherNotLoadException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Weather Service
 */
public class WeatherService {

    /**
     * Current weather instance
     */
    private static WeatherForecast weatherForecast;
    
    /**
     * weatherApiKey String (http://openweathermap.org/appid)
     */
    private String weatherApiKey;

    public WeatherService(String weatherApiKey) {
        this.weatherApiKey = weatherApiKey;
    }

    /**
     * This method returns the current weather information and cache the result 
     * for 70 seconds because the openweather api has a rate limit
     *
     * @param city String
     * 
     * @return WeatherForecast for the next 5 days includes weather data every 3 hours
     */
    public WeatherForecast getCurrentWeatherForecast(String city) {
        if (!(weatherForecast != null
                && weatherForecast.getDate().getTime() >= (new Date().getTime() - 70000))
                || weatherForecast == null) {
            try {
                weatherForecast = loadWeather(city);
            } catch (WeatherNotLoadException ex) {
                Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return weatherForecast;
    }

    /**
     * This Methods calls the new weather information from openweather.com, 
     * as response we will get a json and call convertJsonToWeather 
     * 
     * @param city String  for example Stuttgart,DE
     * 
     * @return WeatherForecast for the next 5 days includes weather data every 3 hours
     * 
     * @throws WeatherNotLoadException if the openweather api not available
     */
    private WeatherForecast loadWeather(String city) throws WeatherNotLoadException {
        StringBuilder jsonResponse = new StringBuilder();

        try {
            URL url = new URL(
                    "http://api.openweathermap.org/data/2.5/forecast/city?q="
                    + URLEncoder.encode(city, "UTF-8")
                    + "&APPID="
                    + this.weatherApiKey
            );

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(url.openStream(), "UTF-8")
            );
            for (String line; (line = reader.readLine()) != null;) {
                jsonResponse.append(line);
            }
        } catch (MalformedURLException | UnsupportedEncodingException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (jsonResponse.length() > 0) {
            return convertJsonToWeather(jsonResponse.toString());
        } else {
            throw new WeatherNotLoadException();
        }
    }

    /**
     * Convert the json response to wetherforecast object
     *
     * @param jsonWeather JSON response of the openweathermap.org api
     * 
     * @return WeatherForecast for the next 5 days includes weather data every 3 hours
     * 
     * @throws WeatherNotLoadException if the openweathermap.org api not available
     */
    private static WeatherForecast convertJsonToWeather(String jsonWeather) throws WeatherNotLoadException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(jsonWeather);
            JsonNode listNode = rootNode.path("list");

            List<WeatherForecastItem> items = new LinkedList<>();
            WeatherForecast forcast = new WeatherForecast(new Date(), items);
            forcast.setCityName(rootNode.path("city").path("name").asText());

            Iterator<JsonNode> forecast = listNode.elements();
            while (forecast.hasNext()) {
                JsonNode item = forecast.next();
                items.add(
                        new WeatherForecastItem(
                                new Date(item.path("dt").asLong() * 1000),
                                item.path("main").path("temp").asDouble() - 273.15,
                                item.path("weather").elements().next().path("description").toString().replace("\"", ""),
                                item.path("clouds").path("all").asInt()
                        )
                );
            }
            return forcast;
        } catch (IOException ex) {
            Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new WeatherNotLoadException();
    }
}
