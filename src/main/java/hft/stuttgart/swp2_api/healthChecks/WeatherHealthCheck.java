package hft.stuttgart.swp2_api.healthChecks;

import com.codahale.metrics.health.HealthCheck;

/**
 * The WeatherHealthCheck checked the given weatherApiKey in the config file
 *
 * @author Andreas Strange, Jasper Eumann
 */
public class WeatherHealthCheck extends HealthCheck {

    /**
     * weatherApiKey String
     */
    private final String weatherApiKey;

    /**
     * Constructor of the WeatherHealthCheck class
     *
     * @param weatherApiKey String
     */
    public WeatherHealthCheck(String weatherApiKey) {
        this.weatherApiKey = weatherApiKey;
    }

    /**
     * Methode checks if a weatherapikey is set in the config file
     * 
     * @return Result of the check
     * 
     * @throws Exception java.lang.Exception 
     */
    @Override
    protected Result check() throws Exception {
        if (weatherApiKey != null && weatherApiKey.length() > 0) {
            return Result.healthy();
        } else {
            return Result.unhealthy("WeatherApiKey not set!");
        }
    }

}
