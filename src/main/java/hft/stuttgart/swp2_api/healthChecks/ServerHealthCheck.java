package hft.stuttgart.swp2_api.healthChecks;

import com.codahale.metrics.health.HealthCheck;

/**
 * The ServerHealthCheck checked the given host and port in the config file 
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class ServerHealthCheck extends HealthCheck {

    /**
     * host String
     */
    private final String host;
    
    /**
     * port int
     */
    private final int port;

    /**
     * Constructor of the ServerHealthCheck class
     * 
     * @param host String
     * @param port int
     */
    public ServerHealthCheck(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * This method checked if the given port and host 
     * 
     * @return Result of the check
     * 
     * @throws Exception Exception
     */
    @Override
    protected Result check() throws Exception {
        if (this.host != null && this.host.length() > 0 && 0 <= this.port) {
            return Result.healthy();
        } else {
            return Result.unhealthy("Host or port not set!");
        }
    }

}
