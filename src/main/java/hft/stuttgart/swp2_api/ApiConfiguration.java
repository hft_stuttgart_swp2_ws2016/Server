package hft.stuttgart.swp2_api;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.bundles.assets.AssetsBundleConfiguration;
import io.dropwizard.bundles.assets.AssetsConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Configuration class the the application
 *
 * @author Andreas Strange, Jasper Eumann
 */
public class ApiConfiguration extends Configuration implements AssetsBundleConfiguration {

    /**
     * Host of the application
     */
    @Valid
    @NotNull
    private String host;

    /**
     * Port of the application
     */
    @Min(1)
    @Max(65535)
    private int port = 8080;

    /**
     * WeatherApiKey for http://openweathermap.org/api
     */
    @Valid
    @NotNull
    private String weatherApiKey;

    /**
     * Path to the 3dcity importExporter config
     */
    @Valid
    @NotNull
    private String citydbExporterConfigPath;

    /**
     * Path to the importExporter jar
     */
    @Valid
    @NotNull
    private String citydbExporterJarPath;

    /**
     * Postgres DatabaseFactory
     */
    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    /**
     * SwaggerBundleConfiguration
     */
    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    /**
     * AssetsConfiguration to set the gltf folder
     */
    @Valid
    @NotNull
    @JsonProperty
    private final AssetsConfiguration assets = AssetsConfiguration.builder().build();

    /**
     * Get host
     *
     * @return host String
     */
    @JsonProperty
    public String getHost() {
        return host;
    }

    /**
     * Set host
     *
     * @param host String
     */
    @JsonProperty
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Get port
     *
     * @return Port int
     */
    @JsonProperty
    public int getPort() {
        return port;
    }

    /**
     * Set port
     *
     * @param port int
     */
    @JsonProperty
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get weatherApiKey
     *
     * @return weatherapikey String
     */
    @JsonProperty
    public String getWeatherApiKey() {
        return weatherApiKey;
    }

    /**
     * Set weatherApiKey
     *
     * @param weatherApiKey String
     */
    @JsonProperty
    public void setWeatherApiKey(String weatherApiKey) {
        this.weatherApiKey = weatherApiKey;
    }

    /**
     * Set dataSourceFactory
     *
     * @param factory database DataSourceFactory
     */
    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    /**
     * Get DataSourceFactory
     *
     * @return database DataSourceFactory
     */
    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    /**
     * Get assetsConfiguration
     *
     * @return AssetsConfiguration
     */
    @Override
    public AssetsConfiguration getAssetsConfiguration() {
        return assets;
    }

    /**
     * Get citydbExporterConfigPath
     *
     * @return citydbExporterConfigPath String
     */
    @JsonProperty
    public String getCitydbExporterConfigPath() {
        return citydbExporterConfigPath;
    }

    /**
     * Set citydbExporterConfigPath
     *
     * @param citydbExporterConfigPath String
     */
    @JsonProperty
    public void setCitydbExporterConfigPath(String citydbExporterConfigPath) {
        this.citydbExporterConfigPath = citydbExporterConfigPath;
    }

    /**
     * Get citydbExporterConfigPath
     *
     * @return citydbExporterConfigPath String
     */
    @JsonProperty
    public String getCitydbExporterJarPath() {
        return citydbExporterJarPath;
    }

    /**
     * Set citydbExporterJarPath
     *
     * @param citydbExporterJarPath String
     */
    @JsonProperty
    public void setCitydbExporterJarPath(String citydbExporterJarPath) {
        this.citydbExporterJarPath = citydbExporterJarPath;
    }
}
