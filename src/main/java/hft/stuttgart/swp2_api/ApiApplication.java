package hft.stuttgart.swp2_api;

import hft.stuttgart.swp2_api.filter.CORSResponseFilter;
import hft.stuttgart.swp2_api.healthChecks.ServerHealthCheck;
import hft.stuttgart.swp2_api.healthChecks.WeatherHealthCheck;
import hft.stuttgart.swp2_api.resource.CafeResource;
import hft.stuttgart.swp2_api.resource.CityResource;
import hft.stuttgart.swp2_api.resource.WeatherForecastResource;
import io.dropwizard.Application;
import io.dropwizard.bundles.assets.ConfiguredAssetsBundle;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import java.util.EnumSet;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

/**
 * Main class of the application
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class ApiApplication extends Application<ApiConfiguration> {

    /**
     * Main method of the api application
     *
     * @param args Commandline args
     * @throws Exception Exception
     */
    public static void main(String[] args) throws Exception {
        new ApiApplication().run(args);
    }

    /**
     * Run methode and place to register resources and set configurations
     *
     * @param configuration Configuration
     * @param environment Eviroment
     */
    @Override
    public void run(ApiConfiguration configuration, Environment environment) {

        //Set Cross-Origin-Header
        environment.jersey().register(CORSResponseFilter.class);
        
        //Set Cross-Origin-Header to serve gltf folder
        Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter("allowedOrigins", "*");
        filter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.setInitParameter("allowedMethods", "GET,PUT,POST,DELETE,OPTIONS");
        System.out.println(System.getProperty("java.home"));
        //Add database
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "postgresql");

        //Register config healthChecks
        environment.healthChecks().register(
                "server",
                new ServerHealthCheck(
                        configuration.getHost(),
                        configuration.getPort()
                )
        );
        environment.healthChecks().register(
                "weather",
                new WeatherHealthCheck(
                        configuration.getWeatherApiKey()
                )
        );

        //Register resources
        final CafeResource resource = new CafeResource(
                jdbi,
                configuration.getCitydbExporterJarPath(),
                configuration.getCitydbExporterConfigPath(),
                configuration.getAssetsConfiguration().getOverrides().get("/gltf")
        );
        environment.jersey().register(resource);

        final WeatherForecastResource weatherForecastResource
                = new WeatherForecastResource(configuration.getWeatherApiKey());
        environment.jersey().register(weatherForecastResource);

        final CityResource cityResource = new CityResource(configuration.getWeatherApiKey());
        environment.jersey().register(cityResource);
    }

    /**
     * Methode to add configuration parameters at runtime of the application
     *
     * @param bootstrap Bootstrap
     */
    @Override
    public void initialize(Bootstrap<ApiConfiguration> bootstrap) {
        bootstrap.addBundle(new ConfiguredAssetsBundle());
        bootstrap.addBundle(new SwaggerBundle<ApiConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ApiConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

}
