package hft.stuttgart.swp2_api.exceptions;

/**
 * WeatherNotLoadException class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class WeatherNotLoadException extends Exception {

    /**
     * Constructor of the WeatherNotLoadException class
     */
    public WeatherNotLoadException() {
    }
    
}
