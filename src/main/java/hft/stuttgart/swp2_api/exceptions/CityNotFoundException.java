package hft.stuttgart.swp2_api.exceptions;

/**
 * City exception class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CityNotFoundException extends Exception {

    /**
     * Constructor of the CityNotFoundException class
     */
    public CityNotFoundException() {
    }

}
