package hft.stuttgart.swp2_api.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;

/**
 * The CORSResponseFilter defined the Cross-Origin-Header
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CORSResponseFilter implements ContainerResponseFilter {
    
    /**
     * Override of filter method of the ContainerResponseFilter class
     * 
     * @param requestContext ContainerRequestContext
     * @param responseContext ContainerResponseContext
     * 
     * @throws IOException IOException
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        MultivaluedMap<String, Object> headers = responseContext.getHeaders();

        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, ");
        headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia, X-Custom-Header");
    }

}
