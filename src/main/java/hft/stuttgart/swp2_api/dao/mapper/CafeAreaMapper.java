package hft.stuttgart.swp2_api.dao.mapper;

import hft.stuttgart.swp2.util.CoordinateTransformation;
import hft.stuttgart.swp2_api.data.Coordinate;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * ResultSetMapper to map cafe areas
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CafeAreaMapper implements ResultSetMapper<Coordinate[]> {

    /**
     * Mapper methode for the cafearea envelope
     *
     * @param index int 
     * @param r ResultSet
     * @param ctx StatementContext
     * 
     * @return Cafe Area as Coordinate[] 
     * 
     * @throws SQLException SQLException
     */
    @Override
    public Coordinate[] map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        String[] envelope = r.getString("envelope").split(";");
        Coordinate[] coordinates = new Coordinate[envelope.length];

        for (int i = 0; i < envelope.length; i++) {
            String[] tmpCoor = envelope[i].trim().split(",");
            coordinates[i] = CoordinateTransformation.gaussKrugerToWgs84(
                    Double.parseDouble(tmpCoor[0]),
                    Double.parseDouble(tmpCoor[1])
            );
        }

        return coordinates;
    }
}
