package hft.stuttgart.swp2_api.dao.mapper;

import hft.stuttgart.swp2.util.CoordinateTransformation;
import hft.stuttgart.swp2_api.data.Address;
import hft.stuttgart.swp2_api.data.Cafe;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * ResultSetMapper to map cafe's
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CafeMapper implements ResultSetMapper<Cafe> {

    /**
     * Mapper methode for cafe objects
     *
     * @param index int 
     * @param r ResultSet
     * @param ctx StatementContext
     * 
     * @return Cafe
     * 
     * @throws SQLException SQLException
     */
    @Override
    public Cafe map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        Cafe cafes;
        String xal = r.getString("xal_source").replaceAll("\n", "");
        int zip = 0;
        String city = "";

        String[] cityArray = xal.substring(
                xal.indexOf("<xAL:LocalityName>") + 18,
                xal.indexOf("</xAL:LocalityName>")
        ).split(" ");
        if (cityArray.length > 1) {
            zip = Integer.parseInt(cityArray[1]);
            city = !"null".equals(cityArray[0]) ? cityArray[0] : "Stuttgart";
        } else {
            city = !"null".equals(cityArray[0]) ? cityArray[0] : "Stuttgart";
        }

        cafes = new Cafe(
                xal.substring(
                        xal.indexOf("<BuildingName>") + 14,
                        xal.indexOf("</BuildingName>")
                ),
                r.getInt("id"),
                CoordinateTransformation.gaussKrugerToWgs84(r.getDouble("lat_coordinate"),r.getDouble("long_coordinate")),
                "/gltf/" + r.getString("id") + ".json",
                new Address(
                        xal.substring(
                                xal.indexOf("<xAL:ThoroughfareName>") + 22,
                                xal.indexOf("</xAL:ThoroughfareName>")
                        ),
                        xal.substring(
                                xal.indexOf("<xAL:ThoroughfareNumber>") + 24,
                                xal.indexOf("</xAL:ThoroughfareNumber>")
                        ),
                        city,
                        zip
                )
        );

        return cafes;
    }
}
