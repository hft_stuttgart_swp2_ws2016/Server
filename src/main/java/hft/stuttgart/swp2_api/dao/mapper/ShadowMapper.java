package hft.stuttgart.swp2_api.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * ResultSetMapper to map shadow as hashmap with percent and timestamp as key
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class ShadowMapper implements ResultSetMapper<HashMap<String, Number>> {

    /**
     * Mapper methode for shadow objects
     *
     * @param index int
     * @param r ResultSet
     * @param ctx StatementContext
     * 
     * @return HasMap with percent and timestamp as key
     * 
     * @throws SQLException SQLException
     */
    @Override
    public HashMap<String, Number> map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        HashMap<String, Number> map = new HashMap<>();
        
        map.put("percent", r.getDouble("schattenprozent"));
        map.put("timestamp", r.getTimestamp("datum").getTime());

        return map;
    }
}
