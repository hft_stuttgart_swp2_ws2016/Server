package hft.stuttgart.swp2_api.dao;

import hft.stuttgart.swp2_api.dao.mapper.CafeMapper;
import hft.stuttgart.swp2_api.dao.mapper.CafeWithShadowMapper;
import hft.stuttgart.swp2_api.data.Cafe;
import java.util.List;
import org.joda.time.DateTime;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

/**
 * Interface to select and create cafes
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public interface CafeDao {

    /**
     * Select cafe`s by given address string
     *
     * @param address String
     * @return List of Cafe Objects
     */
    @SqlQuery("select * from citydb.cafe where xal_source ILike :address")
    @Mapper(CafeMapper.class)
    public List<Cafe> findCafeByAddress(
            @Bind("address") String address
    );

    /**
     * Select cafe by given coordinates
     *
     * @param lat double
     * @param lon double
     * 
     * @return List of Cafe Objects
     */
    @SqlQuery("select * from citydb.cafe where long_coordinate = :lon and lat_coordinate = :lat")
    @Mapper(CafeMapper.class)
    public List<Cafe> findCafeByLatLon(
            @Bind("lat") double lat,
            @Bind("lon") double lon
    );

    /**
     * Select all cafe`s
     *
     * @return List of Cafe Objects
     */
    @SqlQuery("select * from citydb.cafe")
    @Mapper(CafeMapper.class)
    public List<Cafe> findCafes();

    /**
     * Select cafe`s by given address string and start end timestamp
     *
     * @param address String
     * @param start DateTime
     * @param end DateTime
     * 
     * @return List of Cafe Objects
     */
    @SqlQuery("select c.* from citydb.cafe c, citydb.schatten s, "
            + "citydb.cafe_flaeche cf where c.xal_source ILike :address and "
            + "c.id = cf.cafe_id"
            + " and s.fid = cf.id and s.datum >= :start and s.datum <= :end "
            + "and s.schattenprozent < 0.5 group by c.id")
    @Mapper(CafeWithShadowMapper.class)
    public List<Cafe> findCafesByAddressAndSun(
            @Bind("address") String address,
            @Bind("start") DateTime start,
            @Bind("end") DateTime end
    );

    /**
     * Select cafe`s by given start end end timestamp
     *
     * @param start DateTime
     * @param end DateTime
     * 
     * @return List of Cafe Objects
     */
    @SqlQuery("select c.* from citydb.cafe c, citydb.schatten s, "
            + "citydb.cafe_flaeche cf where c.id = cf.cafe_id"
            + " and s.fid = cf.id and s.datum >= :start and s.datum <= :end "
            + "and s.schattenprozent < 0.5 group by c.id")
    @Mapper(CafeWithShadowMapper.class)
    public List<Cafe> findCafesBySun(
            @Bind("start") DateTime start,
            @Bind("end") DateTime end
    );

    /**
     * Select cafe`s by given start end end timestamp and area
     *
     * @param x1 double
     * @param y1 double
     * @param y2 double
     * @param x2 double
     * 
     * @param start DateTime
     * @param end DateTime
     * 
     * @return List of Cafe Objects
     */
    @SqlQuery("select c.* from citydb.cafe c, citydb.schatten s, "
            + "citydb.cafe_flaeche cf where ST_Intersects("
            + "ST_MakeEnvelope(:x1,:y1,:x2,:y2), "
            + "ST_MakePoint(c.long_coordinate, c.lat_coordinate)) and "
            + "c.id = cf.cafe_id"
            + " and s.fid = cf.id and s.datum >= :start and s.datum <= :end "
            + "and s.schattenprozent < 0.5 group by c.id")
    @Mapper(CafeWithShadowMapper.class)
    public List<Cafe> findCafeByLatLonAndSun(
            @Bind("x1") double x1,
            @Bind("y1") double y1,
            @Bind("x2") double x2,
            @Bind("y2") double y2,
            @Bind("start") DateTime start,
            @Bind("end") DateTime end
    );

    /**
     * Select cafe`s by area
     *
     * @param x1 double
     * @param y1 double
     * @param x2 double
     * @param y2 double
     * 
     * @return List of Cafe Objects
     */
    @SqlQuery("select c.* from citydb.cafe c where ST_Intersects(ST_MakeEnvelope(:x1,:y1,:x2,:y2), "
            + "ST_MakePoint(c.long_coordinate, c.lat_coordinate))")
    @Mapper(CafeMapper.class)
    public List<Cafe> findCafeByLatLon(
            @Bind("x1") double x1,
            @Bind("y1") double y1,
            @Bind("x2") double x2,
            @Bind("y2") double y2
    );

    /**
     * Select cafe`s by given address string, start end timestamp and and
     * area
     *
     * @param address String
     * @param x1 double
     * @param y1 double
     * @param x2 double
     * @param y2 double
     * @param start DateTime
     * @param end DateTime
     *
     * @return List of Cafe Objects
     */
    @SqlQuery("select c.* from citydb.cafe c, citydb.schatten s where c.xal_source ILike :address and c.id = s.oid"
            + " and s.datum >= :start and s.datum <= :end and s.schattenprozent < 0.5 "
            + "and ST_Intersects(ST_MakeEnvelope(:x1,:y1,:x2,:y2), "
            + "ST_MakePoint(c.long_coordinate, c.lat_coordinate)) group by c.id")
    
    @Mapper(CafeWithShadowMapper.class)
    public List<Cafe> findCafesByAddressAndLatLonAndSun(
            @Bind("address") String address,
            @Bind("start") DateTime start,
            @Bind("end") DateTime end,
            @Bind("x1") double x1,
            @Bind("y1") double y1,
            @Bind("x2") double x2,
            @Bind("y2") double y2
    );

    /**
     * Select cafe`s by given address string and area
     *
     * @param address String
     * @param x1 double
     * @param y1 double
     * @param x2 double
     * @param y2 double
     *
     * @return List of Cafe Objects
     */
    @SqlQuery("select c.* from citydb.cafe c where c.xal_source ILike :address"
            + "and ST_Intersects(ST_MakeEnvelope(:x1,:y1,:x2,:y2), "
            + "ST_MakePoint(c.long_coordinate, c.lat_coordinate)) group by c.id")
    @Mapper(CafeMapper.class)
    public List<Cafe> findCafesByAddressAndLatLon(
            @Bind("address") String address,
            @Bind("x1") double x1,
            @Bind("y1") double y1,
            @Bind("x2") double x2,
            @Bind("y2") double y2
    );

    /**
     * Method to insert a new cafe
     *
     * @param xalSource String
     * @param latCoordinate double
     * @param longCoordinate double
     * 
     * @return Cafe id
     */
    //todo: insert statement and db schema
    @SqlUpdate("insert into citydb.cafe (xal_source, long_coordinate, lat_coordinate) values "
            + "(:xalSource, :longCoordinate, :latCoordinate)")
    @GetGeneratedKeys
    public long insertCafe(
            @Bind("xalSource") String xalSource,
            @Bind("longCoordinate") double longCoordinate,
            @Bind("latCoordinate") double latCoordinate
    );
}
