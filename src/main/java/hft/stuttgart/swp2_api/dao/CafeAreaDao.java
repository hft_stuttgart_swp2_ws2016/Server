package hft.stuttgart.swp2_api.dao;

import hft.stuttgart.swp2_api.dao.mapper.CafeAreaMapper;
import hft.stuttgart.swp2_api.data.Coordinate;
import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

/**
 * Interface to select and create cafeareas
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public interface CafeAreaDao {

    /**
     * Select cafe`s by given address string and envelope
     *
     * @param cafeId long
     * 
     * @return List of Cafe Objects
     */
    @SqlQuery("select envelope from citydb.cafe_flaeche where cafe_id = :cafeId")
    @Mapper(CafeAreaMapper.class)
    public List<Coordinate[]> findCafesByAddressAndLatLon(
            @Bind("cafeId") long cafeId
    )
;
    /**
     * Method to insert a new cafearea
     *
     * @param cafeId long
     * @param envelope String
     * 
     * @return Cafe id
     */
    @SqlUpdate("insert into citydb.cafe_flaeche (cafe_id, envelope) values "
            + "(:cafeId, :envelope)")
    @GetGeneratedKeys
    public long insertCafe(
            @Bind("cafeId") long cafeId,
            @Bind("envelope") String envelope
    );
}
