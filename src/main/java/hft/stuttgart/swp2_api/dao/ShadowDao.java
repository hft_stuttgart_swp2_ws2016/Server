package hft.stuttgart.swp2_api.dao;

import hft.stuttgart.swp2_api.dao.mapper.ShadowMapper;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

/**
 * Interface to select cafe's with shadow
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public interface ShadowDao {

    /**
     * Select cafe`s by given address string and start and end timestamp
     *
     * @param cafeId long
     * @param start DateTime
     * @param end DateTime
     * @return List of Cafe Objects
     */
    @SqlQuery("select datum, avg(schattenprozent) as schattenprozent "
            + "from citydb.schatten where fid in "
            + "(select id from citydb.cafe_flaeche where cafe_id = :cafeId) "
            + "and datum >= :start and datum <= :end group by datum "
            + "order by datum")
    @Mapper(ShadowMapper.class)
    public List<HashMap<String, Number>> getShadowByCafeIdAndStartEnd(
            @Bind("cafeId") long cafeId,
            @Bind("start") DateTime start,
            @Bind("end") DateTime end
    );

}
