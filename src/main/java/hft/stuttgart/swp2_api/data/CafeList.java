package hft.stuttgart.swp2_api.data;

import java.util.List;

/**
 * Cafelist data class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CafeList {
    
    /**
     * CafeList id
     */
    private long id;
    
    /**
     * Lsit of cafe's
     */
    private List<Cafe> cafes;

    /**
     * Constructor for the cafelist class
     * 
     * @param cafes List cafe's
     */
    public CafeList(List<Cafe> cafes) {
        this.cafes = cafes;
    }

    /**
     * Constructor for the cafelist class
     * 
     * @param id long
     * @param cafes  List of Cafe
     */
    public CafeList(long id, List<Cafe> cafes) {
        this.id = id;
        this.cafes = cafes;
    }
    
    /**
     * Get id
     * 
     * @return id long
     */
    public long getId() {
        return id;
    }

    /**
     * Set id
     * 
     * @param id long
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get cafe's
     * 
     * @return cafes List of Cafe
     */
    public List<Cafe> getCafes() {
        return cafes;
    }

    /**
     * Set cafe's
     * 
     * @param cafes List of Cafe
     */
    public void setCafes(List<Cafe> cafes) {
        this.cafes = cafes;
    }

    /**
     * ToString
     * 
     * @return CafeList String
     */
    @Override
    public String toString() {
        return "CafeList{" + "id=" + id + ", cafes=" + cafes + '}';
    }
    
}
