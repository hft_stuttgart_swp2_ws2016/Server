package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * City class extends the data of a singel city for the weather request.
 *
 * @author Andreas Strange, Jasper Eumann
 */
public class City {

    /**
     * Id long use to count the response
     */
    private long id;

    /**
     * Name String
     */
    private String name;

    /**
     * coordinate coordinate
     */
    private Coordinate coordinate;

    /**
     * CityId int
     */
    private int cityId;

    /**
     * Constructor for the City class
     *
     * @param name String
     * @param coordinate Coordinate
     * @param cityId int
     */
    public City(String name, Coordinate coordinate, int cityId) {
        this.name = name;
        this.cityId = cityId;
        this.coordinate = coordinate;
    }

    /**
     * Get cityId
     *
     * @return cityId int
     */
    @JsonProperty
    public int getCityId() {
        return cityId;
    }

    /**
     * Set cityId
     *
     * @param cityId int
     */
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    /**
     * Get id
     * 
     * @return  id long
     */
    @JsonProperty
    public long getId() {
        return id;
    }

    /**
     * Set id
     * 
     * @param id long
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get name
     * 
     * @return name String
     */
    @JsonProperty
    public String getName() {
        return name;
    }

    /**
     * Set name
     * 
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get coordinate
     * 
     * @return coordinate Coordinate
     */
    @JsonProperty
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     * Set coordinate
     * 
     * @param coordinate Coordinate
     */
    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * ToString
     * 
     * @return  city String
     */
    @Override
    public String toString() {
        return "City{" + "name=" + name + ", coordinate=" + coordinate + '}';
    }

}
