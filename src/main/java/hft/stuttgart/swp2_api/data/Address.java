package hft.stuttgart.swp2_api.data;
/**
 * Address data class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class Address {
    
    /**
     * Street 
     */
    private String street;
    
    /**
     * Housenumber
     */
    private String houseNumber;
    
    /**
     * City
     */
    private String city;
    
    /**
     * Zip
     */
    private int zip;

    /**
     * Constructor of the address class
     * 
     * @param street String
     * @param houseNumber String
     * @param city String
     * @param zip int
     */
    public Address(String street, String houseNumber, String city, int zip) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.city = city;
        this.zip = zip;
    }

    /**
     * Empty constructor of the address class
     */
    public Address() {
    }

    /**
     * Get street
     * 
     * @return street String
     */
    public String getStreet() {
        return street;
    }

    /**
     * Set street
     * 
     * @param street String
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Get houseNumber
     * 
     * @return houseNumber String
     */
    public String getHouseNumber() {
        return houseNumber;
    }

    /**
     * Set houseNumber
     * 
     * @param houseNumber String
     */
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    /**
     * Get city
     * 
     * @return city String
     */
    public String getCity() {
        return city;
    }

    /**
     * Set city
     * 
     * @param city String
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get zip
     * 
     * @return zip int
     */
    public int getZip() {
        return zip;
    }

    /**
     * Set zip
     * 
     * @param zip int
     */
    public void setZip(int zip) {
        this.zip = zip;
    }

    /**
     * ToString method
     * 
     * @return String
     */
    @Override
    public String toString() {
        return "Address{" + "street=" + street + ", houseNumber=" + houseNumber + ", city=" + city + ", zip=" + zip + '}';
    }
}
