package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * Weatheritem class extends a singel forecast with the time for the forecast, 
 * the current temperature at the time, a forcast message and a percentage of clouds
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class WeatherForecastItem {

    /**
     * time date
     */
    private Date time;
    
    /**
     * currentTemp double in degree celsius
     */
    private double currentTemp;
    
    /**
     * forecast String
     */
    private String forecast;
    
    /**
     * clouds int in percent
     */
    private int clouds;

    /**
     * Constructor for the WeatherForecastItem class
     * 
     * @param time Date 
     * @param currentTemp double
     * @param forecast String
     * @param clouds int
     */
    public WeatherForecastItem(Date time, double currentTemp, String forecast, int clouds) {
        this.time = time;
        this.setCurrentTemp(currentTemp);
        this.forecast = forecast;
        this.clouds = clouds;
    }

    /**
     * Get time
     * 
     * @return time Date
     */
    @JsonProperty
    public Date getTime() {
        return time;
    }

    /**
     * Set date
     * 
     * @param time Date
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * Get currentTemp
     * 
     * @return currentTemp double
     */
    @JsonProperty
    public double getCurrentTemp() {
        return currentTemp;
    }

    /**
     * Set currentTemp
     * 
     * @param currentTemp double
     */
    private void setCurrentTemp(double currentTemp) {
        BigDecimal bd = new BigDecimal(currentTemp).setScale(2, RoundingMode.FLOOR);
        this.currentTemp = bd.doubleValue();
    }

    /**
     * Get forecast
     * 
     * @return forecast String
     */
    @JsonProperty
    public String getForecast() {
        return forecast;
    }

    /**
     * Set forecast
     * 
     * @param forecast String
     */
    public void setForecast(String forecast) {
        this.forecast = forecast;
    }

    /**
     * Get clouds
     * 
     * @return clouds int 
     */
    @JsonProperty
    public int getClouds() {
        return clouds;
    }

    /**
     * Set clouds
     * 
     * @param clouds int
     */
    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    /**
     * ToString
     * 
     * @return WeatherForecastItem String
     */
    @Override
    public String toString() {
        return "WeatherForecastItem{" + "time=" + time + ", currentTemp=" + currentTemp + ", forecast=" + forecast + ", clouds=" + clouds + '}';
    }
}
