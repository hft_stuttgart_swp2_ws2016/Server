package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;

/**
 * Weather data class extends a id, a date of the forecast and a list of weatherforcastitems.
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class WeatherForecast {
    
    /**
     * id long
     */
    private long id;
    
    /**
     * date Date
     */
    private Date date;
    
    /**
     * CityName String
     */
    private String cityName;
    
    /**
     * WeatherForcastItems list of WeatherForecastItem
     */
    private List<WeatherForecastItem> weatherForcastItems;

    /**
     * Constructor for the WeatherForecast class
     * 
     * @param date Date 
     * @param weatherForcastItems List of WeatherForecastItem
     */
    public WeatherForecast(Date date, List<WeatherForecastItem> weatherForcastItems) {
        this.date = date;
        this.weatherForcastItems = weatherForcastItems;
    }
    
    /**
     * Get id
     * 
     * @return id long
     */
    @JsonProperty
    public long getId() {
        return id;
    }

    /**
     * Set id
     * 
     * @param id long
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get date
     * 
     * @return date Date
     */
    @JsonProperty
    public Date getDate() {
        return date;
    }

    /**
     * Set Date 
     * 
     * @param date Date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get CityName
     * 
     * @return cityName String
     */
    @JsonProperty
    public String getCityName() {
        return cityName;
    }

    /**
     * Set cityName
     * 
     * @param cityName String
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * Get weatherForcastItems
     * 
     * @return weatherForcastItems List of WeatherForecastItem
     */
    @JsonProperty
    public List<WeatherForecastItem> getWeatherForcastItems() {
        return weatherForcastItems;
    }

    /**
     * Set weatherForcastItems
     * 
     * @param weatherForcastItems List of WeatherForecastItem
     */
    public void setWeatherForcastItems(List<WeatherForecastItem> weatherForcastItems) {
        this.weatherForcastItems = weatherForcastItems;
    }

    /**
     * ToString
     * 
     * @return weatherForecast String
     */
    @Override
    public String toString() {
        return "WeatherForecast{" + "date=" + date + ", weatherForcastItems=" + weatherForcastItems + '}';
    }
    
    
}
