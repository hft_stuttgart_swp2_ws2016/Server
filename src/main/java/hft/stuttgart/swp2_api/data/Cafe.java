package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Coffee data class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class Cafe extends AbstractCafe {

    /**
     * Cafe id
     */
    private int id;
    
    /**
     * Path to the gltf file
     */
    private String serverGltfPathUrl;

    /**
     * Constructor for the cafe class
     *
     * @param serverGltfPathUrl String
     * @param name String
     * @param locationXY Coordinate
     * @param id int
     * @param address Address
     */
    public Cafe(String name, int id, Coordinate locationXY, String serverGltfPathUrl, Address address) {
        super(name, locationXY, address);
        this.id = id;
        this.serverGltfPathUrl = serverGltfPathUrl;
    }

    /**
     * Get id
     * 
     * @return id int
     */
    @JsonProperty
    public int getId() {
        return id;
    }

    /**
     * Set id
     * 
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get serverGltfPathUrl
     * 
     * @return serverGltfPathUrl String
     */
    @JsonProperty
    public String getServerGltfPathUrl() {
        return serverGltfPathUrl;
    }

    /**
     * Set serverGltfPathUrl
     * 
     * @param serverGltfPathUrl String
     */
    public void setServerGltfPathUrl(String serverGltfPathUrl) {
        this.serverGltfPathUrl = serverGltfPathUrl;
    }

    /**
     * ToString
     * 
     * @return cafe String
     */
    @Override
    public String toString() {
        return "Cafe{" + "name=" + super.getName() + ", gmlId=" + id + ", locationXY=" + super.getLocationXY()+ ", serverGltfPathUrl=" + serverGltfPathUrl + '}';
    }

}
