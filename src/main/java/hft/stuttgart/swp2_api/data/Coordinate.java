package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Coordinate data class extends long and lat values 
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class Coordinate {
    
    /**
     * longCoordinate double
     */
    private double longCoordinate;
    
    /**
     * latCoordinate double
     */
    private double latCoordinate;

    /**
     * Constructor for the coordinate class
     * 
     * @param longCoordinate double
     * @param latCoordinate double
     */
    public Coordinate(double longCoordinate, double latCoordinate) {
        this.longCoordinate = longCoordinate;
        this.latCoordinate = latCoordinate;
    }

    /**
     * Emty constructor for the coordinate class
     */
    public Coordinate() {
    }
    
    /**
     * Get longCoordinate
     * 
     * @return longCoordinate double
     */
    @JsonProperty
    public double getLongCoordinate() {
        return longCoordinate;
    }

    /**
     * Set longCoordinate
     * 
     * @param longCoordinate double
     */
    public void setLongCoordinate(double longCoordinate) {
        this.longCoordinate = longCoordinate;
    }
    
    /**
     * Get latCoordinate
     * 
     * @return latCoordinate double
     */
    @JsonProperty
    public double getLatCoordinate() {
        return latCoordinate;
    }

    /**
     * Set latCoordinate
     * 
     * @param latCoordinate double
     */
    public void setLatCoordinate(double latCoordinate) {
        this.latCoordinate = latCoordinate;
    }

    /**
     * ToString
     * 
     * @return coordinate String
     */
    @Override
    public String toString() {
        return "Coordinate{" + "longCoordinate=" + longCoordinate + ", latCoordinate=" + latCoordinate + '}';
    }
}
