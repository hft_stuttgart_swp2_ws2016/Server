package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Coffee data class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class AbstractCafe {

    /**
     * Name of the cafe
     */
    private String name;
    
    /**
     * Coordinate of the cafe
     */
    private Coordinate locationXY;
    
    /**
     * Address of the cafe
     */
    private Address address;

    /**
     * Constructor for the cafe class
     *
     * @param name String
     * @param locationXY Coordinate
     * @param address Address
     */
    public AbstractCafe(String name, Coordinate locationXY,Address address) {
        this.name = name;
        this.locationXY = locationXY;
        this.address = address;
    }
    
    /**
     * Emty constructor for the cafe class
     */
    public AbstractCafe() {
    }

    /**
     * Get name
     * 
     * @return String
     */
    @JsonProperty
    public String getName() {
        return name;
    }

    /**
     * Set name
     * 
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get Address
     * 
     * @return Address
     */
    @JsonProperty
    public Address getAddress() {
        return address;
    }

    /**
     * Set address
     * 
     * @param address Address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Get getLocationXY
     * 
     * @return Coordinate
     */
    @JsonProperty
    public Coordinate getLocationXY() {
        return locationXY;
    }

    /**
     * Set locationXY
     * 
     * @param coordinate Coordinate
     */
    public void setLocationXY(Coordinate coordinate) {
        this.locationXY = coordinate;
    }

    /**
     * ToString methode
     * 
     * @return String
     */
    @Override
    public String toString() {
        return "Cafe{" + "name=" + name + ", locationXY=" + locationXY + '}';
    }

}
