package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.List;

/**
 * NewCoffee data class
 * 
 * This class can be used to add new cafes
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class NewCafe extends AbstractCafe {

    /**
     * envelopes as List of coordinate arrays
     */
    private List<Coordinate[]> envelopes;

    /**
     * Constructor for the NewCoffee class
     *
     * @param name String
     * @param coordinate Coordinate
     * @param envelopes List of Coordinate arrays
     * @param address Address
     */
    public NewCafe(String name, Coordinate coordinate, Address address, List<Coordinate[]> envelopes) {
        super(name, coordinate, address);
        this.envelopes = envelopes;
    }

    /**
     * Emtty Constructor for the NewCoffee class
     */
    public NewCafe() {
        super();
    }

    /**
     * Get envelopes
     * 
     * @return envelopes List of Coordinate arrays
     */
    @JsonProperty
    public List<Coordinate[]> getEnvelopes() {
        return envelopes;
    }

    /**
     * Set envelopes
     * 
     * @param envelopes List of Coordinate arrays
     */
    @JsonProperty
    public void setEnvelopes(List<Coordinate[]> envelopes) {
        this.envelopes = envelopes;
    }

    /**
     * ToString
     * 
     * @return NewCafe String
     */
    @Override
    public String toString() {
        StringBuilder enve = new StringBuilder();
        envelopes.forEach((c) -> {
            enve.append(Arrays.toString(c));
        });
        return "Cafe{" + "name=" + super.getName() + ", envelope=" + enve.toString() + ", locationXY=" + super.getLocationXY() + ", address=" + super.getAddress() + '}';
    }

}
