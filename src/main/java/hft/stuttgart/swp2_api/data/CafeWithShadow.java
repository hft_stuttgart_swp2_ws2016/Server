package hft.stuttgart.swp2_api.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.List;

/**
 * CafeWithShadow data class
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CafeWithShadow extends Cafe {

    /**
     * List of HashMap with shadow informations
     */
    private List<HashMap<String, Number>> shadow;
    
    /**
     * Cafe areas as List of Coordinate arrays 
     */
    private List<Coordinate[]> envelopes;

    /**
     * Constructor for the CafeWithShadow class
     * 
     * @param name String
     * @param id int
     * @param locationXY Coordinate
     * @param serverGltfPathUrl String
     * @param address Address
     * @param envelopes List of Coordinate arrays
     */
    public CafeWithShadow(String name, int id, Coordinate locationXY, String serverGltfPathUrl, Address address, List<Coordinate[]> envelopes) {
        super(name, id, locationXY, serverGltfPathUrl, address);
        this.envelopes = envelopes;
    }
    

    /**
     * Get shadow
     * 
     * @return shadow List of HashMap
     */
    @JsonProperty
    public  List<HashMap<String, Number>> getShadow() {
        return shadow;
    }

    /**
     * Set shadow
     * 
     * @param shadow List of HashMap
     */
    @JsonProperty
    public void setShadow(List<HashMap<String, Number>> shadow) {
        this.shadow = shadow;
    }

    /**
     * Get envelopes
     * 
     * @return envelopes List Coordinate arrays
     */
    @JsonProperty
    public List<Coordinate[]> getEnvelopes() {
        return envelopes;
    }

    /**
     * Set envelopes
     * 
     * @param envelopes List of Coordinate arrays
     */
    @JsonProperty
    public void setEnvelopes(List<Coordinate[]> envelopes) {
        this.envelopes = envelopes;
    }
    
    
}
