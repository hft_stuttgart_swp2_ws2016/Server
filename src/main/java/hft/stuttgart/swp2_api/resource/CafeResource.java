package hft.stuttgart.swp2_api.resource;

import com.codahale.metrics.annotation.Timed;
import hft.stuttgart.swp2_api.data.CafeList;
import hft.stuttgart.swp2_api.data.NewCafe;
import hft.stuttgart.swp2_api.services.CafeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import org.skife.jdbi.v2.DBI;

/**
 * The CafeResource class defines all request methods on the /cafelist path
 *
 * @author Andreas Strange, Jasper Eumann
 */
@Api("/cafelist")
@Path("/cafelist")
@Produces(MediaType.APPLICATION_JSON)
public class CafeResource {

    /**
     * counter AtomicLong counts the requests
     */
    private final AtomicLong counter;
    
    /**
     * cafeService CafeService instance
     */
    private CafeService cafeService;
    
    /**
     * importerExporterJar String path to the 3dcitydb importExporterJar 
     */
    private String importerExporterJar;
    
    /**
     * config String path to the 3dcitydb importExporterJar config
     */
    private String config;
    
    /**
     * gltfFolder String path to the export folder
     */
    private String gltfFolder;

    /**
     * Constructor of the CafeResource class
     * 
     * @param dbi DBI
     * @param importerExporter String
     * @param config String
     * @param gltfFolder String
     */
    public CafeResource(DBI dbi, String importerExporter, String config, String gltfFolder) {
        this.counter = new AtomicLong();
        this.cafeService = new CafeService(dbi);
        this.importerExporterJar = importerExporter;
        this.config = config;
        this.gltfFolder = gltfFolder;
    }

    /**
     * This method is called from a GET request and
     * returns all the cafes that are available
     *
     * @param name String
     * @param address String
     * @param lat double
     * @param lon double
     * @param radius int
     * @param unit String m or km
     * @param begin long unix timestamp
     * @param end long unix timestamp
     * 
     * @return CafeList or 404 Cafe not found
     */
    @GET
    @Timed
    @ApiOperation(value = "Find Cafe's by name, address or area")
    @ApiResponses(value = {
        @ApiResponse(code = 404, message = "Cafe not found")})
    public CafeList cafeList(
            @ApiParam(value = "Cafe name")
            @QueryParam("name") String name,
            @ApiParam(value = "Cafe address (street 1, city)")
            @QueryParam("address") String address,
            @ApiParam(value = "Latitude")
            @QueryParam("lat") double lat,
            @ApiParam(value = "Longitude")
            @QueryParam("lon") double lon,
            @ApiParam(value = "Radius")
            @QueryParam("radius") int radius,
            @ApiParam(value = "Radius unit (m or km default m)")
            @QueryParam("unit") String unit,
            @ApiParam(value = "Date interval start for a place in the sun (Timestamp)")
            @QueryParam("begin") long begin,
            @ApiParam(value = "Date interval end for a place in the sun (Timestamp) if begin is set end is default start + 3 days")
            @QueryParam("end") long end
    ) {
        CafeList cafeList = this.getCafeService().getCafeList(name, address, lat, lon, radius, unit, begin, end);

        if (cafeList != null && cafeList.getCafes().size() > 0) {
            cafeList.setId(counter.incrementAndGet());
            return cafeList;
        } else {
            throw new WebApplicationException("Cafe not found", 404);
        }
    }

    /**
     * This method is called from a POST request and insert a new cafe
     * 
     * @param cafe NewCafe
     * 
     * @return Response 201 or 404 Cafe not insert
     */
    @POST
    @Timed
    @ApiOperation(value = "Add a cafe")
    public Response postObservations(
            @ApiParam(value = "Cafe object")
            @Valid NewCafe cafe
    ) {
        System.out.println("NewCafe: " + cafe.toString());
        if (this.getCafeService().addCafe(cafe, this.importerExporterJar, this.config, this.gltfFolder)) {
            return Response.created(UriBuilder.fromResource(CafeResource.class)
                    .build(cafe))
                    .build();
        } else {
            throw new WebApplicationException("Cafe not insert", 404);
        }

    }

    /**
     * Get cafeService
     * 
     * @return cafeService CafeService
     */
    public CafeService getCafeService() {
        return cafeService;
    }
}
