package hft.stuttgart.swp2_api.resource;

import com.codahale.metrics.annotation.Timed;
import hft.stuttgart.swp2_api.data.City;
import hft.stuttgart.swp2_api.services.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
/**
 * This Class returns a city for API path "/city"
 * 
 * @author Andreas Strange, Jasper Eumann
 */
@Api("/city")
@Path("/city")
@Produces(MediaType.APPLICATION_JSON)
public class CityResource {

    /**
     * counter AtomicLong counts the requests
     */
    private final AtomicLong counter;
    
    /**
     * cityService CityService
     */
    private CityService cityService;
    
    /**
     * The consturctor of CityResource class
     * 
     * @param weatherApiKey the openweather API Key
     */
    public CityResource(String weatherApiKey) {
        this.counter = new AtomicLong();
        this.cityService = new CityService(weatherApiKey);
    }

    
    /**
     * This method trys to return a city for a given Name, in case of a failure it returns a 404/500 Error Code
     * 
     * @param name of the city to look up
     * 
     * @return a city
     */
    @GET
    @Timed
    @ApiOperation(value = "Get city information from openweathermap.org")
    @ApiResponses(value = {
        @ApiResponse(code = 404, message = "City information not available!")})
    public City searchCity(@NotNull @QueryParam("name") @ApiParam(value = "City name, country code") String name) {
        City city = cityService.getCityByQuery(name);
        if (city != null) {
            city.setId(counter.incrementAndGet());
            return city;
        } else {
            throw new WebApplicationException("City information not available!", 500);
        }
    }
}
