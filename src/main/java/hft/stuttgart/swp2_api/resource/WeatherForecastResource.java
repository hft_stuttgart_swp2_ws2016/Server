package hft.stuttgart.swp2_api.resource;

import com.codahale.metrics.annotation.Timed;
import hft.stuttgart.swp2_api.data.WeatherForecast;
import hft.stuttgart.swp2_api.services.WeatherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.constraints.NotNull;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

/**
 * The WeatherForecastResource class returns for the "/weatherforecast" api-path a weatherforecast for a city
 * 
 */
@Api("/weatherforecast")
@Path("/weatherforecast")
@Produces(MediaType.APPLICATION_JSON)
public class WeatherForecastResource {

    /**
     * weatherService WeatherService
     */
    private WeatherService weatherService = null;
    
    /**
     * counter AtomicLong counts the requests
     */
    private final AtomicLong counter;

    /**
     * The constructor of the weatherForecastResource class
     * 
     * @param weatherApiKey String (http://openweathermap.org/appid)
     */
    
    public WeatherForecastResource(String weatherApiKey) {
        this.counter = new AtomicLong();
        this.weatherService = new WeatherService(weatherApiKey);

    }

    /**
     * This method looks up a weatherforecast for a city
     * 
     * @param city a in the weatherService looked up city
     * 
     * @return a weatherforecast for 5 days within a intervall of 2 hours
     */
    
    @GET
    @Timed
    @ApiOperation(value = "Get weatherforcast from given city name")
    @ApiResponses(value = {
        @ApiResponse(code = 404, message = "Weather information not available!")})
    public Object returnWeatherForcast(@NotNull @QueryParam("city") @ApiParam(value = "City name, country code") String city) {
        WeatherForecast forecast = weatherService.getCurrentWeatherForecast(city);
        if (forecast != null) {
            forecast.setId(counter.incrementAndGet());
            return forecast;
        } else {
            throw new WebApplicationException("Weather information not available!", 500);
        }
    }
}
//
