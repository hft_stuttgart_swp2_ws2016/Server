package hft.stuttgart.swp2.util;

import hft.stuttgart.swp2_api.data.Coordinate;

/**
 * Helper class for exporting gltf files from 3dcitydb
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class GltfExportUtil {

    /**
     * Path to the 3dcity import-expoter jar file
     */
    private final String importerExporterJar;
    /**
     * Path to the 3dcity import-exporter config file
     */
    private final String config;
    /**
     * Export folder path
     */
    private final String gltfFolder;
    
    private DoThread helperThread;

    /**
     * Standard constructor of the GltfExportUtil class
     *
     * @param importerExporterJar String
     * @param config String
     * @param gltfFolder String
     */
    public GltfExportUtil(String importerExporterJar, String config, String gltfFolder) {
        this.importerExporterJar = importerExporterJar;
        this.config = config;
        this.gltfFolder = gltfFolder;
        this.helperThread = null;
    }

    /**
     * Method to create gltf-files in a bounding box
     *
     * @param cafeId long
     * @param boundingBox Array of Coordinates 
     */
    public void createGltfFiles(long cafeId, Coordinate[] boundingBox) {
        new Thread(new DoThread(cafeId, boundingBox, importerExporterJar, config, gltfFolder)).start();
    }
}
