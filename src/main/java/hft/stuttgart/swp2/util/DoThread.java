package hft.stuttgart.swp2.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import hft.stuttgart.swp2_api.data.Coordinate;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Helper thread to create gltf files
 */
public class DoThread implements Runnable {

    /**
     * Path to the 3dcity import-expoter jar file
     */
    private final String importerExporterJar;
    /**
     * Path to the 3dcity import-exporter config file
     */
    private final String config;
    /**
     * Export folder path
     */
    private final String gltfFolder;
    /**
     * Cafe id
     */
    private final long cafeId;
    /**
     * Array of coordinates around the cafe
     */
    private final Coordinate[] boundingBox;

    /**
     * Standard constructor
     *
     * @param cafeId cafe id
     * @param boundingBox array of coordinates around the cafe
     * @param importerExporter path to the 3dcity import-expoter jar file
     * @param config Path to the 3dcity import-exporter config file
     * @param gltfFolder Export folder path
     */
    DoThread(long cafeId, Coordinate[] boundingBox, String importerExporter, String config, String gltfFolder) {
        this.cafeId = cafeId;
        this.boundingBox = boundingBox;
        this.importerExporterJar = importerExporter;
        this.config = config;
        this.gltfFolder = gltfFolder;
    }

    /**
     * Run method of the helper thread class to create the gltf files The method
     * get the area of the bounding box and all buildings who lay inside this
     * area Then a new file, which includes all the paths to the gltfs file,
     * will be created
     */
    @Override
    public void run() {
        try {
            File sourceConfig = new File(this.config);
            File destConfig = new File(this.config + "." + cafeId + ".xml");
            //copy config to tmp config
            Files.copy(sourceConfig.toPath(), destConfig.toPath());

            setBoundingBox(boundingBox, destConfig);
            String javaHome = System.getProperty("java.home");

            String javaBin = javaHome + "/bin/java";
            String exportPath = this.gltfFolder + "/" + cafeId;
            String tilesPath = this.gltfFolder + "/Tiles/0/0/" + cafeId + "_Tile_0_0_collada.kml";
            boolean isWindows = false;

            if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                isWindows = true;
                javaBin = javaHome + "\\bin\\java.exe";
                exportPath = this.gltfFolder + "\\" + cafeId;
                tilesPath = this.gltfFolder + "\\Tiles\\0\\0\\" + cafeId + "_Tile_0_0_collada.kml";
            }

            System.out.println(javaBin + " " + exportPath + " " + " " + tilesPath + " " + isWindows + " " + " " + this.config + " " + this.importerExporterJar);
            ProcessBuilder pb = new ProcessBuilder(
                    javaBin,
                    "-jar",
                    this.importerExporterJar,
                    "-shell",
                    "-config", this.config + "." + cafeId + ".xml",
                    "-kmlExport", exportPath
            );

            Process p = pb.start();
            

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            System.out.println(builder.toString());
            p.waitFor();
            ArrayList<HashMap<String, String>> gltfFiles = new ArrayList<>();

            File file = new File(tilesPath.trim());
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            NodeList placemarks = document.getElementsByTagName("Placemark");

            for (int i = 0; i < placemarks.getLength(); i++) {
                Node n = placemarks.item(i).getChildNodes().item(3);
                HashMap<String, String> map = new HashMap<>();
                map.put("long", n.getChildNodes().item(3).getChildNodes().item(1).getTextContent().trim());
                map.put("lat", n.getChildNodes().item(3).getChildNodes().item(3).getTextContent().trim());
                map.put("orientation", n.getChildNodes().item(5).getChildNodes().item(1).getTextContent().trim());
                map.put("file", "gltf/Tiles/0/0/" + n.getChildNodes().item(7).getChildNodes().item(1).getTextContent().replace(".dae", ".gltf").trim());

                gltfFiles.add(map);
            }

            //remove unused file
            File file1;
            File file2;

            if (isWindows) {
                file1 = new File(this.gltfFolder + "\\" + cafeId + ".kml");
                file2 = new File(this.gltfFolder + "\\" + cafeId + "_collada_MasterJSON.json");
            } else {
                file1 = new File(this.gltfFolder + "/" + cafeId + ".kml");
                file2 = new File(this.gltfFolder + "/" + cafeId + "_collada_MasterJSON.json");
            }

            //delete unused files
            file.delete();
            file1.delete();
            file2.delete();
            destConfig.delete();

            ObjectMapper mapper = new ObjectMapper();
            if (isWindows) {
                mapper.writeValue(new File((this.gltfFolder + "\\" + cafeId + ".json").trim()), gltfFiles);
            } else {
                mapper.writeValue(new File(this.gltfFolder + "/" + cafeId + ".json"), gltfFiles);
            }

        } catch (SAXException | IOException | ParserConfigurationException | InterruptedException | TransformerException | DOMException | TransformerFactoryConfigurationError ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    /**
     * Set the BoundingBox in 3dcitydb exporter config
     *
     * @param boundingBox array of coordinates
     * @param destConfig destination config as file
     *
     * @throws SAXException SAXException
     * @throws ParserConfigurationException ParserConfigurationException
     * @throws TransformerConfigurationException
     * TransformerConfigurationException
     * @throws TransformerException TransformerException
     * @throws IOException IOException
     * @throws DOMException DOMException
     * @throws TransformerFactoryConfigurationError
     * TransformerFactoryConfigurationError
     */
    private void setBoundingBox(Coordinate[] boundingBox, File destConfig) throws SAXException, ParserConfigurationException, TransformerConfigurationException, TransformerException, IOException, DOMException, TransformerFactoryConfigurationError {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(destConfig);
        NodeList nodes = doc.getChildNodes().item(0).getChildNodes();
        NodeList filter = nodes.item(7).getChildNodes().item(3).getChildNodes().item(5).getChildNodes();

        for (int j = 0; j < filter.getLength(); j++) {
            Node n = filter.item(j);
            if (n.getNodeName().equals("boundingBox")) {
                //lowerCorner
                n.getChildNodes().item(1).setTextContent(
                        boundingBox[3].getLatCoordinate()
                        + " " + boundingBox[0].getLongCoordinate()
                );
                //upperCorner
                n.getChildNodes().item(3).setTextContent(
                        boundingBox[2].getLatCoordinate()
                        + " " + boundingBox[1].getLongCoordinate()
                );
            }
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(destConfig);
        transformer.transform(source, result);
    }
}
