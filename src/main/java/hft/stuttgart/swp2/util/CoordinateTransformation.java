/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hft.stuttgart.swp2.util;

import hft.stuttgart.swp2_api.data.Coordinate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.geometry.GeneralDirectPosition;
import org.geotools.referencing.CRS;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

/**
 * Class to convert wgs84 coordinates to gauss-krueger coordinates
 * 
 * @author Andreas Strange, Jasper Eumann
 */
public class CoordinateTransformation {

    /**
     * Method to convert a coordinate from wgs84 to Gauss Krueger
     *
     * @param lon double
     * @param lat double
     * 
     * @return Coordinate in Gauss-Krueger or null
     */
    public static Coordinate wgs84ToGaussKrueger(double lon, double lat) {
        try {
            CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4326");
            CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:31467");

            MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, true);
            DirectPosition expPt = new GeneralDirectPosition(lat, lon);
            expPt = transform.transform(expPt, null);
            return new Coordinate(expPt.getCoordinate()[0], expPt.getCoordinate()[1]);
        } catch (FactoryException | MismatchedDimensionException | TransformException ex) {
            Logger.getLogger(CoordinateTransformation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * Method to convert a coordinate from Gauss Krueger to wgs84
     *
     * @param lon double
     * @param lat double
     * 
     * @return Coordinate in wgs84 or null
     */
    public static Coordinate gaussKrugerToWgs84(double lon, double lat) {
        try {
            CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:31467");
            CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:4326");

            MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, true);
            DirectPosition expPt = new GeneralDirectPosition(lat, lon);
            expPt = transform.transform(expPt, null);
            return new Coordinate(expPt.getCoordinate()[1], expPt.getCoordinate()[0]);
        } catch (FactoryException | MismatchedDimensionException | TransformException ex) {
            Logger.getLogger(CoordinateTransformation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
}
